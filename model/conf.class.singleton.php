<?php
    class conf {
        private $userdb;
        private $passdb;
        private $hostdb;
        private $db;
        static $_instance;
    
        private function __construct() {
            $cnfg = parse_ini_file(MODEL_PATH."/bd.ini");
            $this->userdb = $cnfg['user'];
            $this->passdb = $cnfg['password'];
            $this->hostdb = $cnfg['host'];
            $this->db = $cnfg['db'];
            
        }
    
        private function __clone() {
            
        }
    
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }
       
    }