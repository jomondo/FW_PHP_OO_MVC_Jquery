<?php
    function amigable($url, $return = false) {
        $amigableson = URL_AMIGABLES;
        $link = "";
        if ($amigableson) {
            $url = explode("&", str_replace("?", "", $url));
            foreach ($url as $key => $value) {
                $aux = explode("=", $value);
                $link .=  $aux[1]."/";
            }
        } else {
            $link = "index.php" . $url;

        }
        if ($return) {
            return SITE_PATH . $link;

        }
        
        echo SITE_PATH . $link;
    }

    function amigable1($url, $return = false) {
        $amigableson = URL_AMIGABLES;
        $link = "";
        $i = 0;
        if ($amigableson) {
            $url = explode("&", str_replace("?", "", $url));
            foreach ($url as $key => $value) {
                $aux = explode("=", $value);

                $link .= "/" . $aux[1];
            }
        } else {
            $link = "/index.php" . $url;
        }
        if ($return) {
            return SITE_PATH .$link;
        }
        echo SITE_PATH . $link;
    }

    function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);

        return utf8_encode($cadena);

    }
    
    /*function get_gravatar( $email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array() ){
        $email = trim($email);
        $email = strtolower($email);
        $email_hash = md5($email);
        
        $url = "https://www.gravatar.com/avatar/".$email_hash;
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }*/

    function get_gravatar(){
        $rand = rand(1,20);
        return $rand;
    }

    ///////// destroy session /////////
    function close_session() {
        unset($_SESSION['users']);
        unset($_SESSION['result_avatar']);
        $_SESSION = array(); 
        session_destroy();
        
    }