Proyecto Freelancer
===================

- Contenido de clase hasta el momento:
	1. template nuevo
	2. 2 formularios
	3. validaciones js y php
	4. dropzone
	5. combos dependents
	6. list/details
	7. scroll categorias en home
	8. paginacion en list_categorias y autocomplete
	9. autocomplete por click y por keyword
	10. paths
	11. pretty
	12. router
	13. autoload
	14. enviar email mailgun
	15. google maps ( markers, infoWindow )
	16. gravatar
	17. login y signup ( redes sociales y manual )
	18. Recover password
	19. Update profile

- Mejoras:
	1. breadcrumb
	2. combos dependents ( api y json )
	3. scroll ( obtener categorias con bd y respaldo json )
	4. mostrar las 4 categorías más visitadas
	5. mostrar estadísticas en home ( clients, freelancer )
	6. filtros en list
	7. funcion load_data_ajax ( optimización de peticiones ajax )
	8. widgets jquery ( catcomplet, checkboxradio, selectmenu, slider [ precio min y max ] )
	9. amaran toaster
	10. rating stars en home
	11. activación subrayado menu
	12. autocarga includes utils de las clases en router
	13. modificar codigo google maps a jquery
	14. funcion normaliza utf8 ( decodificador )
	15. datos user encriptados en localStorage con json web tokens ( base64 )
	16. rating stars freelancer en details ( control de votación por usuario ) y mostrar los 6 freelancer mejor votados en home
	

Tecnologías usadas
==================

- Bases de datos: SQL
- Almacenamiento y manipulación de datos: JSON y XML

- Backend:
	1. PHP

- Frontend:
	1. Javascript
	2. Jquery

- Diseño web:
	1. HTML5
	2. CSS3
	3. Bootstrap
