$('document').ready(function() {
    $( "#change_passwd" ).on( "click", function() {
        $("form#register").css("display","none");
        $("form#login").css("display","none");
        $("#btn_register").css("display","none");
        $("form#change_password").css("display","block");
        //$(".social_network").css("display","none");

    });

    $(document).on('click','#btn_passwd',function(){
		var rdo = true;
        
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var email = $("#email_passwd").val();
        
        if( email == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;
        }else if (!emailreg.test(email)){
            var message = "Tiene que ser un email valido. ejemplo@gmail.com";
            amaran(message);
            rdo=false;
            return false;
        }

        if (rdo) {
            var data = { "email":email };
            var data_JSON = JSON.stringify(data);
            console.log("json debug: "+data_JSON);
            ///// comprobacion email en db /////

            $.post(amigable('?module=login&function=check_email'),{data: data_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                if (json.success) {
                	localStorage.setItem("verify_email",true);
                	window.location.href = json.redirect;
                }else if(json.msg){
                	amaran(json.msg);
                	return false;

                }
                
               }).fail(function(xhr){
                	//alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }

           });//end fail

        }//end rdo

	});

    $(document).on('click','#btn_change',function(){
        var rdo = true;
        
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var passwdreg = /^[A-Za-z0-9ñÑ]{5,20}$/;
        var email = $("#email_change").val();
        var passwd1 = $("#password1").val();
        var passwd2 = $("#password2").val();
        
        if( email == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;
        }else if (!emailreg.test(email)){
            var message = "Tiene que ser un email valido. ejemplo@gmail.com";
            amaran(message);
            rdo=false;
            return false;
        }if( passwd1 == ""){
            var message = "Introduce su password";
            amaran(message);
            rdo=false;
            return false;
        }else if (!passwdreg.test(passwd1)){
            var message = "El password permite alfanumerico de 5-20 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( passwd1 !== passwd2){
            var message = "El password no coincide";
            amaran(message);
            rdo=false;
            return false;
        }

        if (rdo) {
            var data = { "email":email, "password": passwd1 };
            var data_JSON = JSON.stringify(data);
            console.log("json debug: "+data_JSON);
            ///// comprobacion email en db /////

            $.post(amigable('?module=login&function=change_passwd'),{data: data_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                if (json.success) {
                    localStorage.setItem("change_passwd",true);
                    window.location.href = json.redirect;
                }else if(json.error){
                    amaran(json.error);
                    return false;

                }
                
               }).fail(function(xhr){
                    //alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }

           });//end fail

        }//end rdo

    });

});