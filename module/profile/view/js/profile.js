$(document).ready(function(){
    $("#formClient_update").css('display','none');
    $("#formAutonomo_update").css('display','none');
	$("#formClient").css('display','none');
	$("#formAutonomo").css('display','none');
    $("#titleMatters").css('display','none');
    $("#titleSpecialist").css('display','none');

	//client
	$("#btnClient").click(function(){
		$("#formAutonomo").css('display','none');
		$("#formClient").css('display','block');

		//////// dependents form client ////////////
        var id_community_c = "community";
        var id_province_c = "province";
        var id_city_c = "city";
        combosDepenents(id_community_c,id_province_c,id_city_c);

	});

	//autonomo
	$("#btnAutonmo").click(function(){
		$("#formClient").css('display','none');
		$("#formAutonomo").css('display','block');

        //////// dependents form freelancer ////////////
        var id_community_c = "community_free";
        var id_province_c = "province_free";
        var id_city_c = "city_free";
        combosDepenents(id_community_c,id_province_c,id_city_c);

	});

    checkboksStyle();
	//validations
	validateForms();

	////// datepickers //////////
    $('#birthday').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#expirationDni').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#birthday_update').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#expirationDni_update').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#date_up_free').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#birthday_free').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#date_up_free_update').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    $('#birthday_free_update').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2018',

    });

    //checkboxradio jqueryui
    $( "input:checkbox[name=checkbox]" ).checkboxradio();
    inputsDependents();

    /* ----- security checking data ------ */
    //module/profile/controller/controller_profile.class.php?security=true
    $.ajax({
        url: '../profile/security/',
        type: 'GET',
        success:function(response){
            alert(response);
            var json = JSON.parse(response);
            if (json.user===null) {
                $("#dni").val('');
                $("#name").val('');
                $("#last_name").val('');
                $("#birthday").val('');
                $("#expirationDni").val('');
                $("#email").val('');
                $("#phone").val('');
                $("#community").val('');
                $("#province").val('');
                $("#city").val('');
                deleteCheckboks();

            }else{
                $("#dni").val(json.user.dni);
                $("#name").val(json.user.name);
                $("#last_name").val(json.user.last_name);
                $("#birthday").val(json.user.birthday);
                $("#expirationDni").val(json.user.expirationDni);
                $("#email").val(json.user.email);
                $("#phone").val(json.user.phone);
                $("#community").val(json.user.community);
                $("#province").val(json.user.province);
                $("#city").val(json.user.city);
                var inputElements = document.getElementsByClassName('hidden');
                for (var i = 0; i < inputElements.length; i++) {
                    if (inputElements[i].checked) {
                        inputElements[i].checked = true;
                    }
                }

            }
           
        },
        error:function(xhr){
            alert(xhr.responseText);
        },


    });
    /////// function get id_user/////////////////////
    var data = localStorage.getItem("datos_user");
    //console.log("asd: "+data);
    var json = JSON.parse(data);
    //console.log("n: "+json);
    var tokenInit = {"token":json.token};
    var tokenToInit = JSON.stringify(tokenInit);

    $.ajax({
        url: amigable('?module=login&function=select_user_token'),
        type: 'POST',
        data:{data:tokenToInit},
            success:function(response){
                //alert();
                //console.log("n: "+response);
                var json = JSON.parse(response);
                if (json.success) {
                    var id = json.user[0].id_user;
                    localStorage.setItem("id",id);
                }
                   
              },
              error:function(xhr){
                alert(xhr.responseText);
              },

    });

    peticion_type();
});//end ready
    function peticion_type(){
        var data = localStorage.getItem("datos_user");
        var json = JSON.parse(data);
        /*var data_user = do_decode(data);
        var json_d = JSON.parse(data_user);*/
        var tokenInit = {"token":json.token};
        var tokenToInit = JSON.stringify(tokenInit);

        $.ajax({
          url: amigable('?module=login&function=select_user_token'),
          type: 'POST',
          data:{data:tokenToInit},
          success:function(response){
            alert("asd: "+response);
            var json_d = JSON.parse(response);
            if (json_d.success) {
                //alert("success");
                if (json_d.user[0].type==0) {//client
                    var data = { "id":json_d.user[0].id_user };
                    var data_id = JSON.stringify(data);
                    
                    $.ajax({
                        url: '../profile/check_type/',
                        type: 'POST',
                        data:{data:data_id},
                        success:function(response){
                            alert(response);
                            //alert("users");
                            var json_f = JSON.parse(response);
                            if (json_f.users) {// si ja te el perfil complet
                                $("#btnClient").css("display","none");
                                $("#btnAutonmo").css("display","none");
                                alert(json_d.user[0].passwd);
                                
                                load_data_ajax("?module=profile&function=get_data_user","POST",data_id,"","get_data_user");

                            }else{
                                amaran("Complete your profile to find what you want");
                                
                                $(document).on('click','#btnClient',function(){
                                    $("input#name").val(json_d.user[0].name);
                                    $("input#email").val(json_d.user[0].email);
                                });

                                $(document).on('click','#btnAutonmo',function(){
                                    $("input#name_free").val(json_d.user[0].name);
                                    $("input#email_free").val(json_d.user[0].email);
                                });
                                
                            }
                            
                           
                        },
                        error:function(xhr){
                            alert(xhr.responseText);
                        },

                    });
                }
            }
               
          },
          error:function(xhr){
            alert(xhr.responseText);
          },

        });
        
        
    }

    function checkboksStyle(){
        $('.button-checkbox').each(function () {
            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
                //alert(isChecked);

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    }

    //delete checkboxs
    function deleteCheckboks(){
        $('.button-checkbox').each(function () {
            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };


            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
                //alert(isChecked);

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

              
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    }
    function inputsDependents (){
        var cat_prof;
        var matters;
        
        ////// search json ///////
        var searchIntoJson = function (obj, column, value) {
            var results = [];
            var valueField;
            var searchField = column;
            for (var i = 0 ; i < obj.length ; i++) {
                valueField = obj[i][searchField].toString();
                if (valueField === value) {
                    results.push(obj[i]);
                }
            }
            return results;
        };

        ////////// load combos ////////
        var loadCat_prof = function (){
            //$("#gamma").empty();
            //$("#cat_prof").append('<select id="categories"></select>');
            $.each(category, function (i, valor) {
                $("#cat_prof").append("<option id='"+valor.name_catg+"' value='" + valor.categoryId + "'>" + valor.name_catg + "</option>");
            });
        };

        var loadMatters = function (category){
            //alert(category);
            var cate_profMaters = searchIntoJson(matters, "categoryId", category);
            $("#matters").empty();
            $("#specialist").empty();
            //$("#matters").append('<h4>Select matters </h4>');
            $("#titleMatters").css('display','block');
            $("#titleSpecialist").css('display','block');
            $.each(cate_profMaters, function (i, valor) {
                $("#matters").append('<input type="checkbox" id="'+valor.nameMatter+'" name="checkboxs" class="checkboxs" value="'+valor.nameMatter+'"> '+valor.nameMatter+' </input>');
                $("#specialist").append('<input type="radio" id="'+valor.nameMatter+'" class="lol" name="radio" class="radio" value="'+valor.nameMatter+'"> '+valor.nameMatter+' </input>');

            });
        };
        
       /////////// carga json externos /////////////////
        $.getJSON("https://localhost/freelancer/resources/categories.json", function (data){
            category = data;

        });

        $.getJSON("https://localhost/freelancer/resources/matters.json", function (data){
            matters = data;
            setTimeout(function () {
                if (matters !== undefined) {
                    loadCat_prof();
                }
            }, 1000);
        });

        //selectmenu jqueryui (combo)
        $( "#cat_prof" ).selectmenu({
            width: 200,
            change: function( event, ui ){
                var category = $("#cat_prof").val();
                //alert(category);
                loadMatters(category);
            }
        });
        
    }// end inputsDependents

    function inputsDependents_update (value){
        var cat_prof;
        var matters;
        
        ////// search json ///////
        var searchIntoJson = function (obj, column, value) {
            var results = [];
            var valueField;
            var searchField = column;
            for (var i = 0 ; i < obj.length ; i++) {
                valueField = obj[i][searchField].toString();
                if (valueField === value) {
                    results.push(obj[i]);
                }
            }
            return results;
        };

        ////////// load combos ////////
        /*var loadCat_prof = function (){
            //$("#gamma").empty();
            //$("#cat_prof").append('<select id="categories"></select>');
            $.each(category, function (i, valor) {
                $("#cat_prof_update").append("<option id='"+valor.name_catg+"' value='" + valor.categoryId + "'>" + valor.name_catg + "</option>");
            });
        };*/

        var loadMatters = function (category){
            //alert(category);
            var cate_profMaters = searchIntoJson(matters, "categoryId", category);
            $("#matters_update").empty();
            $("#specialist_update").empty();
            //$("#matters").append('<h4>Select matters </h4>');
            $("#titleMatters").css('display','block');
            $("#titleSpecialist").css('display','block');
            $.each(cate_profMaters, function (i, valor) {
                $("#matters_update").append('<input type="checkbox" id="'+valor.nameMatter+'" name="checkboxs" class="checkboxs" value="'+valor.nameMatter+'"> '+valor.nameMatter+' </input>');
                $("#specialist_update").append('<input type="radio" id="'+valor.nameMatter+'" class="lol" name="radio" class="radio" value="'+valor.nameMatter+'"> '+valor.nameMatter+' </input>');

            });
        };

       /////////// carga json externos /////////////////
       /* $.getJSON("https://localhost/freelancer/resources/categories.json", function (data){
            category = data;

        });*/

        $.getJSON("https://localhost/freelancer/resources/matters.json", function (data){
            matters = data;
            
        });

        $( "#cat_prof_update" ).change(function() {
            var category = $("#cat_prof_update").val();
            alert("ha canviat: "+category);
            loadMatters(category);
            value.forEach(function(elemento){
                var f = elemento.materia.split(":");
                for(var i=0;i<f.length;i++){
                    alert(f[i]);
                    if (f[i]=="Java") {
                        elemento1 = $("input:checkbox[id=Java]");
                        elemento1.prop("checked", true);
                    }if (f[i]=="Jquery") {
                        elemento = $("input:checkbox[id=Jquery]");
                        elemento.prop("checked", true);
                    }if (f[i]=="PHP") {
                        elemento = $("input:checkbox[id=PHP]");
                        elemento.prop("checked", true);
                    }if (f[i]=="Django") {
                        elemento = $("input:checkbox[id=Django]");
                        elemento.prop("checked", true);
                    }if (f[i]=="Node") {
                        elemento = $("input:checkbox[id=Node]");
                        elemento.prop("checked", true);
                    }if (f[i]=="Angular") {
                        elemento = $("input:checkbox[id=Angular]");
                        elemento.prop("checked", true);
                    }if (f[i]==".NET") {
                        elemento = $("input:checkbox[id=.NET]");
                        elemento.prop("checked", true);
                    }if (f[i]=="Javascript") {
                        elemento = $("input:checkbox[id=Javascript]");
                        elemento.prop("checked", true);
                    }
                                
                }
                        
            });
        });
        
    }// end inputsDependents

/* ----- general combos dependents ----- */
function combosDepenents(id_community,id_province,id_city){
    load_community_v1();

    /* desabilitar province i city mentres no es pulse valor en combo community */
    propCombo(true);
    $("#"+id_community).change(function() {
        var community = $(this).val();
        //alert(community);
        propCombo(false);
        load_provinces_v1(community);
        
    });

    $("#"+id_province).change(function() {
        var prov = $(this).val();
        //alert(prov);
        if(prov > 0){
            load_cities_v1(prov);
        }else{
            $("#city").prop('disabled', false);
        }
    });

    function propCombo (boolean){
        var province = $("#"+id_province);
        var city = $("#"+id_city);
        province.prop('disabled', boolean);
        city.prop('disabled', boolean);
        /*$("#province").empty();
        $("#city").empty();*/

    }
    //module/profile/controller/controller_profile.class.php?load_community=true
    function load_community_v1() {
        $.get( "../profile/load_community/", function(response){
            alert(response);
            if(response === 'error'){
                load_community_v2("resources/community.json");

            }else{
                load_community_v2("../profile/load_community/"); //api json online
    
            }
        }).fail(function(response) {
            load_community_v2("https://localhost/freelancer/resources/community.json");

        });
    }

    function load_community_v2(cad) {
        $.getJSON(cad, function(data) {
            $("#"+id_community).empty();
            $("#"+id_community).append('<option value="" selected="selected">Select community</option>');
            //alert(data.data[1].COM);
            $.each(data.data, function (i, valor) {
                //alert(valor.COM);
                $("#"+id_community).append("<option id="+valor.COM+" value='" + valor.CCOM + "'>" + valor.COM + "</option>");
            });

        }).fail(function() {
            alert( "error load_community client" );

        });
    }

    function load_provinces_v1(id_comunity) {
        var datos = { id_comunity : id_comunity };
        $.post("../profile/load_provinces/",datos,function(response) {
                //alert(response);
                if (response==='error') {
                    load_provinces_v2();
                }else{
                    var json = JSON.parse(response);
                    $("#"+id_province).empty();
                    $("#"+id_province).append('<option value="" selected="selected">Select province</option>');

                    $.each(json.data, function (i, valor) {
                        $("#"+id_province).append("<option id="+valor.PRO+" value='" + valor.CPRO + "'>" + valor.PRO + "</option>");
                    });
                    //load_provinces_v2();//prova search json

                }
                
        }).fail(function(xhr) {
            //alert(xhr.responseText);
            load_provinces_v2();
        });
    }

    /* ----- respaldo json ----- */
    var province;
    var city;
    // search json 
    var search = function (obj, column, value) {
        var results = [];
        var valueField;
        var searchField = column;
            
        for (var i = 0 ; i < obj.length ; i++) {
            valueField = obj[i][searchField].toString();
            //alert(valueField);
            if (valueField === value) {
                results.push(obj[i]);
            }
        }
        return results;
    };

    var loadProvinces = function (){
        var id_comunity = $("#"+id_community).val();      
        var prov = search(province, "CCOM", id_comunity);
                
        $.each(prov, function (i, valor) {
            //alert(valor.PRO);
            $("#"+id_province).append("<option id="+valor.PRO+" value='" + valor.CPRO + "'>" + valor.PRO + "</option>");

        });
    };

    var loadCities = function (){
        var id_prov = $('#'+id_province).val();      
        var prov = search(city, "CPRO", id_prov);
                
        $.each(prov, function (i, valor) {
            //alert(valor.PRO);
            $("#"+id_city).append("<option id="+valor.DMUN50+" value='" + valor.CMUM + "'>" + valor.DMUN50 + "</option>");

        });
    };

    /* get json provinces */
    $.get("https://localhost/freelancer/resources/province.json", function (json) {
        //alert(json);
        province = json;
    });

    /* get json cities */
    $.get("https://localhost/freelancer/resources/city.json", function (json) {
        //alert(json);
        city = json;
    });

    function load_provinces_v2() {
        $.get("https://localhost/freelancer/resources/province.json", function (json) {
            //alert(json);
            //province = json.data;
            $("#"+id_province).empty();
            $("#"+id_province).append('<option value="" selected="selected">Select province</option>');
            loadProvinces();
            
        })
        .fail(function() {
            alert( "error load_provincessss" );
        });
    } 

    function load_cities_v1(prov) {
        var datos = { id_prov : prov };
        $.post("../profile/load_cities/", datos, function(response) {
            //alert(response);
            var json = JSON.parse(response);

            $("#"+id_city).empty();
            $("#"+id_city).append('<option value="" selected="selected">Select city</option>');

            if(response === 'error'){
                load_cities_v2(prov);
            }else{
                $.each(json.data, function (i, valor) {
                    $("#"+id_city).append("<option id="+valor.DMUN50+" value='" + valor.CMUM + "'>" + valor.DMUN50 + "</option>");
                });
                //load_cities_v2(prov);
            }
        }).fail(function() {
            load_cities_v2(prov);
        });
    }

    function load_cities_v2(prov) {
        $.get("https://localhost/freelancer/resources/city.json", function (json) {
            $("#"+id_city).empty();
            $("#"+id_city).append('<option value="" selected="selected">Select city</option>');
            loadCities();
            
        }).fail(function() {
            alert( "error load_cities" );

        });
    }
}  

function validateForms() {
	$(document).on('click','#Client',function(){
        var id_comunity;
		var rdo = true;
        //que la fetxa no siga major a 2018
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var dnireg = /^[0-9]{8}[A-Z]{1}$/;
        var birthdayreg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
        var expdnireg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
        var last_namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var phonereg = /^[0-9]{9}$/;
            
        var dni = $("#dni").val();
        var name = $("#name").val();
        var last_name = $("#last_name").val();
        var birthday = $("#birthday").val();
        var exp_dni = $("#expirationDni").val();
        id_community = $("#community").val();
        if (id_community) {
            var community = $("option[value="+id_community+"]").attr('id');
        }

        var id_province= $("#province").val();
        if (id_province) {
            var province = $("option[value="+id_province+"]").attr('id');
        }
        
        var id_city = $("#city").val();
        if (id_city) {
            var city = $("option[value="+id_city+"]").attr('id');
        }
        
        
        
        let language = [];
        var inputElements = document.getElementsByClassName('hidden');
        var j = 0;
        for (var i = 0; i < inputElements.length; i++) {
            if (inputElements[i].checked) {
                language[j] = inputElements[i].value;
                j++;
            }
        }

        var email = $("#email").val();
        var phone = $("#phone").val();
        var type = $('button#Client').attr('id');
        
        /*var g = $('input[type="checkbox"]').button() var m = $('input[type="checkbox"]').val();       
        var l = $('#checkbox-4').prop('checked', true).button("refresh");*///per al UPDATE!!
        
        if( $("#dni").val() == ""){
            var message = "Introduce el dni";
            amaran(message);
            rdo=false;
            return false;
        }else if (!dnireg.test($("#dni").val())){
            var message = "El dni tiene que tener 8 numeros y una letra";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#name").val() == ""){
            var message = "Introduce el nombre";
            amaran(message);
            rdo=false;
            return false;
        }else if (!namereg.test($("#name").val())){
            var message = "El nombre tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#last_name").val() == ""){
            var message = "Introduce apellidos";
            amaran(message);
            rdo=false;
            return false;
        }else if (!last_namereg.test($("#last_name").val())){
            var message = "Los apellidos tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#birthday").val() == ""){
            var message = "Introduce la fecha";
            amaran(message);
            rdo=false;
            return false;
        }else if (!birthdayreg.test($("#birthday").val())){
            var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#expirationDni").val() == ""){
            var message = "Introduce la fecha de expiracion dni";
            amaran(message);
            rdo=false;
            return false;
        }else if (!expdnireg.test($("#expirationDni").val())){
            var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#email").val() == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;
        }else if (!emailreg.test($("#email").val())){
            var message = "Tiene que ser un email valido. ejemplo@gmail.com";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#phone").val() == ""){
            var message = "Introduce su telefono";
            amaran(message);
            rdo=false;
            return false;
        }else if (!phonereg.test($("#phone").val())){
            var message = "El telefono tiene que tener 9 numeros";
            amaran(message);
            rdo=false;
            return false;
        }if($("#community").val() === "" || $("#community").val() === "Select community" || $("#community").val() === null) {
            var message = "Select one community";
            amaran(message);
            rdo=false;
            return false;//falta validate en PHP
        }if ($("#province").val() === "" || $("#province").val() === "Select province") {
            var message = "Select one province";
            amaran(message);
            rdo=false;
            return false;
        }
        if ($("#city").val() === "" || $("#city").val() === "Select city") {
            var message = "Select one city";
            amaran(message);
            rdo=false;
            return false;
        }
        if( $('input[name="hidden"]:checked').val() == undefined){
            var message = "Selecciona uno o varios idiomas";
            amaran(message);
            rdo=false;
            return false;
        }

        if (rdo) {

            var id = localStorage.getItem("id");
            console.log("el rdo es true en client js");
            var data = { "id":id, "dni":dni, "name":name, "last_name":last_name, "birthday":birthday,"exp_dni":exp_dni, "community":community,"province":province,"city":city, "language":language, "email":email, "phone":phone , "type":type};
            //falta enviar valors de country a data

            var data_profile_JSON = JSON.stringify(data);
            console.log("json debug: "+data_profile_JSON);

            $.post('../profile/alta_profile_client/',{alta_profile_json: data_profile_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                //alert(json.success);
                console.log("success: "+json.success);
                if (json.success) {
                    localStorage.removeItem("id");
                    window.location.href = json.redirect;
                }
                
               }).fail(function(xhr){
                //alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    //alert(xhr.responseJSON);
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }

                    if (xhr.responseJSON.error.validaDni){
                        amaran(xhr.responseJSON.error.validaDni);
                        
                    }

                    if (xhr.responseJSON.error.name){
                        amaran(xhr.responseJSON.error.name);
                     
                    }
                    if (xhr.responseJSON.error.last_name){
                        amaran(xhr.responseJSON.error.last_name);
                        
                    }

                    if (xhr.responseJSON.error.birthday){
                        amaran(xhr.responseJSON.error.birthday);
                        
                    }

                    if (xhr.responseJSON.error.exp_dni){
                        amaran(xhr.responseJSON.error.exp_dni);
                        
                    }

                    if (xhr.responseJSON.error.errorCompare){
                        amaran(xhr.responseJSON.error.errorCompare);
                        
                    }

                    if (xhr.responseJSON.error.language){
                        amaran(xhr.responseJSON.error.language);
                        
                    }

                    if (xhr.responseJSON.error.email){
                        amaran(xhr.responseJSON.error.email);
                        
                    }

                    if (xhr.responseJSON.error.phone){
                        amaran(xhr.responseJSON.error.phone);
                        
                    }

                    if (xhr.responseJSON.error.country){
                        amaran(xhr.responseJSON.error.country);
                        
                    }
                    if(xhr.responseJSON.error.province){
                        amaran(xhr.responseJSON.error.province);

                    }

                    if(xhr.responseJSON.error.city){
                        amaran(xhr.responseJSON.error.city);

                    }

           });//end fail

        }//end rdo

        
	});

	$(document).on('click','#Freelancer',function(){
        var rdo = true;
        
        var dnireg = /^[0-9]{8}[A-Z]{1}$/;
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var last_namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var date_upreg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{4,12}[.]{1}[A-Za-z]{2,4}$/;
        var phonereg = /^[0-9]{9}$/;
        var pricereg = /^[0-9]{1,3}$/;
        var proyectreg = /^[A-Za-z\sñÑ0-9]{15,500}$/;
            
        var dni = $("#dni_free").val();
        //alert(dni);
        var name = $("#name_free").val();
        var last_name = $("#last_name_free").val();
        var price_h = $("#price_free").val();
        var birthday = $("#birthday_free").val();
        var date_up = $("#date_up_free").val();
        var id_community = $("#community_free").val();
        if (id_community) {
            var community = $("option[value="+id_community+"]").attr('id');
        }

        var id_province = $("#province_free").val();
        if (id_province) {
            var province = $("option[value="+id_province+"]").attr('id');
        }

        var id_city = $("#city_free").val();
        if (id_city) {
            var city = $("option[value="+id_city+"]").attr('id');
        }

        let language = [];
        var inputElements = document.getElementsByClassName('checkbox');
        var j = 0;
        for (var i = 0; i < inputElements.length; i++) {
            if (inputElements[i].checked) {
                language[j] = inputElements[i].value;
                j++;
            }
        }
        var id_category = $("#cat_prof").val();
        var category = $("option[value="+id_category+"]").attr('id');
        alert("validate: "+id_category);
        let matters = [];
        var inputElements = document.getElementsByClassName('checkboxs');
        var j = 0;
        for (var i = 0; i < inputElements.length; i++) {
            if (inputElements[i].checked) {
                matters[j] = inputElements[i].value;
                j++;
            }
        }
        var specialist = $("input:radio[name=radio]:checked").val();
        //alert(specialist);
        var proyect = $("#proyect").val();
        var email = $("#email_free").val();
        var phone = $("#phone_free").val();
        var type = $('button#Freelancer').attr('id');

        if( $("#dni_free").val() == ""){
            var message = "Introduce el dni";
            amaran(message);
            rdo=false;
            return false;
        }else if (!dnireg.test($("#dni_free").val())){
            var message = "El dni tiene que tener 8 numeros y una letra";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#name_free").val() == ""){
            var message = "Introduce el nombre";
            amaran(message);
            rdo=false;
            return false;
        }else if (!namereg.test($("#name_free").val())){
            var message = "El nombre tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#last_name_free").val() == ""){
            var message = "Introduce apellidos";
            amaran(message);
            rdo=false;
            return false;
        }else if (!last_namereg.test($("#last_name_free").val())){
            var message = "Los apellidos tiene que tener entre 4-35 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#birthday_free").val() == ""){
            var message = "Introduce la fecha";
            amaran(message);
            rdo=false;
            return false;
        }else if (!date_upreg.test($("#birthday_free").val())){
            var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#date_up_free").val() == ""){
            var message = "Introduce la fecha";
            amaran(message);
            rdo=false;
            return false;
        }else if (!date_upreg.test($("#date_up_free").val())){
            var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#email_free").val() == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;
        }else if (!emailreg.test($("#email_free").val())){
            var message = "Tiene que ser un email valido. ejemplo@gmail.com";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#phone_free").val() == ""){
            var message = "Introduce su telefono";
            amaran(message);
            rdo=false;
            return false;
        }else if (!phonereg.test($("#phone_free").val())){
            var message = "El telefono tiene que tener 9 numeros";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#price_free").val() == ""){
            var message = "Introduce precio/hora";
            amaran(message);
            rdo=false;
            return false;
        }else if (!pricereg.test($("#price_free").val())){
            var message = "Entre 1 y 3 numeros";
            amaran(message);
            rdo=false;
            return false;
        }if($("#community_free").val() === "" || $("#community_free").val() === "Select community" || $("#community_free").val() === null) {
            var message = "Select one community";
            amaran(message);
            rdo=false;
            return false;//falta validate en PHP
        }if ($("#province_free").val() === "" || $("#province_free").val() === "Select province") {
            var message = "Select one province";
            amaran(message);
            rdo=false;
            return false;
        }
        if ($("#city_free").val() === "" || $("#city_free").val() === "Select city") {
            var message = "Select one city";
            amaran(message);
            rdo=false;
            return false;
        }if( $('input[name="checkbox"]:checked').val() == undefined){
            var message = "Selecciona uno o varios idiomas";
            amaran(message);
            rdo=false;
            return false;
        }if( $('#cat_prof').val() == ""){
            var message = "Selecciona una categoria";
            amaran(message);
            rdo=false;
            return false;
        }if( $('input[name="checkboxs"]:checked').val() == undefined){
            var message = "Selecciona materias";
            amaran(message);
            rdo=false;
            return false;
        }if( $('input[type="radio"]:checked').val() == undefined){
            var message = "Selecciona una especialidad";
            amaran(message);
            rdo=false;
            return false;
        }if( $("#proyect").val() == ""){
            var message = "Introduce su proyecto";
            amaran(message);
            rdo=false;
            return false;
        }else if (!proyectreg.test($("#proyect").val())){
            var message = "La caja de proyecto tiene que tener entre 10-500 caracteres";
            amaran(message);
            rdo=false;
            return false;
        }
        
        if (rdo) {
            var data = localStorage.getItem("datos_user");
            var json_c = JSON.parse(data);
            console.log("el rdo es true en client js");
            var data = { "id":json_c.id,"dni":dni, "name":name, "last_name":last_name, "birthday":birthday,"date_up":date_up, "community":community,"province":province,"city":city, "language":language, "category":category,"matters":matters,"specialist":specialist,"proyect":proyect,"email":email, "phone":phone , "price_h":price_h,"type":type };
            
            var data_profile_JSON = JSON.stringify(data);
            console.log("json debug: "+data_profile_JSON);

            $.post('../profile/alta_profile_freelancer/',{alta_profileFreelancer_json: data_profile_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                //alert(json.success);
                console.log("success: "+json.success);
                if (json.success) {
                    window.location.href = json.redirect;
                }
                
               }).fail(function(xhr){
                alert(xhr.responseText);
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    //alert(xhr.responseJSON);
                    if (xhr.responseJSON.msg){
                        amaran(xhr.responseJSON.msg);

                    }

                    if (xhr.responseJSON.error.dni){
                        amaran(xhr.responseJSON.error.dni);
                        
                    }
                    if (xhr.responseJSON.error.validaDni){
                        amaran(xhr.responseJSON.error.validaDni);
                        
                    }

                    if (xhr.responseJSON.error.name){
                        amaran(xhr.responseJSON.error.name);
                     
                    }
                    if (xhr.responseJSON.error.last_name){
                        amaran(xhr.responseJSON.error.last_name);
                        
                    }

                    if (xhr.responseJSON.error.birthday){
                        amaran(xhr.responseJSON.error.birthday);
                        
                    }

                    if (xhr.responseJSON.error.date_up){
                        amaran(xhr.responseJSON.error.date_up);
                        
                    }

                    if (xhr.responseJSON.error.errorCompare){
                        amaran(xhr.responseJSON.error.errorCompare);
                        
                    }

                    if (xhr.responseJSON.error.language){
                        amaran(xhr.responseJSON.error.language);
                        
                    }

                    if (xhr.responseJSON.error.email){
                        amaran(xhr.responseJSON.error.email);
                        
                    }

                    if (xhr.responseJSON.error.phone){
                        amaran(xhr.responseJSON.error.phone);
                        
                    }

                    if (xhr.responseJSON.error.price_h){
                        amaran(xhr.responseJSON.error.price_h);
                        
                    }

                    if (xhr.responseJSON.error.community){
                        amaran(xhr.responseJSON.error.community);
                        
                    }
                    if(xhr.responseJSON.error.province){
                        amaran(xhr.responseJSON.error.province);

                    }

                    if(xhr.responseJSON.error.city){
                        amaran(xhr.responseJSON.error.city);

                    }

                    

                });//end fail

        }//end rdo

	});

    ///////////// dropzone ///////////////
    Dropzone.autoDiscover = false;
    $("#dropzone").dropzone({
        url: "../profile/upload/",
        method: "post",
        withCredentials: false,
        parallelUploads: 1, //Cuanto archivos subir al mismo tiempo
        uploadMultiple: false,
        maxFilesize: 5, //Maximo Tamaño del archivo expresado en mbyts
        paramName: "file",//Nombre con el que se envia el archivo
        createImageThumbnails: true,
        maxThumbnailFilesize: 1, //Limite para generar imagenes (Previsualizacion)
        thumbnailWidth: 154, //Medida de largo de la Previsualizacion
        thumbnailHeight: 154,//Medida alto Previsualizacion
        filesizeBase: 1000,
        maxFiles: 1,//define cuantos archivos se cargan al dropzone. Si se excede, se llamara al evento maxfilesexceeded.
        params: {}, //Parametros adicionales al formulario de envio ejemplo {tipo:"imagen"}
        clickable: true,
        ignoreHiddenFiles: true,
        acceptedFiles: "image/*", //extensiones
        acceptedMimeTypes: null,//Ya no se utiliza, paso a ser AceptedFiles
        autoProcessQueue: true,//True sube las imagenes automaticamente, si es false se tiene que llamar a myDropzone.processQueue(); para subirlas
        autoQueue: true,
        addRemoveLinks: true,//Habilita la posibilidad de eliminar/cancelar un archivo. Las opciones dictCancelUpload, dictCancelUploadConfirmation y dictRemoveFile se utilizan para la redacción.
        previewsContainer: null,//define dónde mostrar las previsualizaciones de archivos. Puede ser un HTMLElement liso o un selector de CSS. El elemento debe tener la estructura correcta para que las vistas previas se muestran correctamente.
        capture: null,
        dictDefaultMessage: "Arrastra los archivos aqui para subirlos",//missatges errors
        dictFallbackMessage: "Su navegador no soporta arrastrar y soltar para subir archivos.",
        dictFallbackText: "Por favor utilize el formuario de reserva de abajo como en los viejos tiempos.",
        dictFileTooBig: "La imagen supera el tamaño permitido ({{filesize}}MiB). Tam. Max : {{maxFilesize}}MiB. No se subira a nuestro servidor",
        dictInvalidFileType: "No se puede subir este tipo de archivos.",
        dictResponseError: "Server responded with {{statusCode}} code.",
        dictCancelUpload: "Cancel subida",
        dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
        dictRemoveFile: "Eliminar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos. No se subira a nuestro servidor",
        success: function(file, message){
            alert(file.status);
            if (file.status=="success") {
                $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente a nuestra base de datos.');
            }

        },
        removedfile: function(file) {
            //alert(file);
            var _ref;
            if (file.previewElement) {
              if ((_ref = file.previewElement) != null) {
                _ref.parentNode.removeChild(file.previewElement);
                var name = file.name;
                $.ajax({
                  type: "POST",
                  url: "../profile/delete/",
                  data: "filename="+name,
                  success:function(){
                    $('.msg').text('');
                    $('.msg').text('El archivo '+name+' se ha eliminado satisfactoriamente');

                  }
                });
                
              }
            }
            return this._updateMaxFilesReachedClass();
        },
        error: function(file,message) {
            //alert(file.status);
            //adecuar be els errors que venen de php
            alert(message);
            amaran(message);

        }//error
    });

    ///////////// dropzone1 ///////////////
    Dropzone.autoDiscover = false;
    $(".dropzone1").dropzone({
        url: "../profile/upload/",
        method: "post",
        withCredentials: false,
        parallelUploads: 1, //Cuanto archivos subir al mismo tiempo
        uploadMultiple: false,
        maxFilesize: 5, //Maximo Tamaño del archivo expresado en mbyts
        paramName: "file",//Nombre con el que se envia el archivo
        createImageThumbnails: true,
        maxThumbnailFilesize: 1, //Limite para generar imagenes (Previsualizacion)
        thumbnailWidth: 154, //Medida de largo de la Previsualizacion
        thumbnailHeight: 154,//Medida alto Previsualizacion
        filesizeBase: 1000,
        maxFiles: 1,//define cuantos archivos se cargan al dropzone. Si se excede, se llamara al evento maxfilesexceeded.
        params: {}, //Parametros adicionales al formulario de envio ejemplo {tipo:"imagen"}
        clickable: true,
        ignoreHiddenFiles: true,
        acceptedFiles: "image/*", //extensiones
        acceptedMimeTypes: null,//Ya no se utiliza, paso a ser AceptedFiles
        autoProcessQueue: true,//True sube las imagenes automaticamente, si es false se tiene que llamar a myDropzone.processQueue(); para subirlas
        autoQueue: true,
        addRemoveLinks: true,//Habilita la posibilidad de eliminar/cancelar un archivo. Las opciones dictCancelUpload, dictCancelUploadConfirmation y dictRemoveFile se utilizan para la redacción.
        previewsContainer: null,//define dónde mostrar las previsualizaciones de archivos. Puede ser un HTMLElement liso o un selector de CSS. El elemento debe tener la estructura correcta para que las vistas previas se muestran correctamente.
        capture: null,
        dictDefaultMessage: "Arrastra los archivos aqui para subirlos",//missatges errors
        dictFallbackMessage: "Su navegador no soporta arrastrar y soltar para subir archivos.",
        dictFallbackText: "Por favor utilize el formuario de reserva de abajo como en los viejos tiempos.",
        dictFileTooBig: "La imagen supera el tamaño permitido ({{filesize}}MiB). Tam. Max : {{maxFilesize}}MiB. No se subira a nuestro servidor",
        dictInvalidFileType: "No se puede subir este tipo de archivos.",
        dictResponseError: "Server responded with {{statusCode}} code.",
        dictCancelUpload: "Cancel subida",
        dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
        dictRemoveFile: "Eliminar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos. No se subira a nuestro servidor",
        success: function(file, message){
            alert(file.status);
            if (file.status=="success") {
                $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente a nuestra base de datos.');
            }

        },
        removedfile: function(file) {
            //alert(file);
            var _ref;
            if (file.previewElement) {
              if ((_ref = file.previewElement) != null) {
                _ref.parentNode.removeChild(file.previewElement);
                var name = file.name;
                $.ajax({
                  type: "POST",
                  url: "../profile/delete/",
                  data: "filename="+name,
                  success:function(){
                    $('.msg').text('');
                    $('.msg').text('El archivo '+name+' se ha eliminado satisfactoriamente');

                  }
                });
                
              }
            }
            return this._updateMaxFilesReachedClass();
        },
        error: function(file,message) {
            //alert(file.status);
            //adecuar be els errors que venen de php
            alert(message);
            amaran(message);

        }
    });

    ///////////// dropzone1 ///////////////
    Dropzone.autoDiscover = false;
    $(".dropzone2").dropzone({
        url: "../profile/upload/",
        method: "post",
        withCredentials: false,
        parallelUploads: 1, //Cuanto archivos subir al mismo tiempo
        uploadMultiple: false,
        maxFilesize: 5, //Maximo Tamaño del archivo expresado en mbyts
        paramName: "file",//Nombre con el que se envia el archivo
        createImageThumbnails: true,
        maxThumbnailFilesize: 1, //Limite para generar imagenes (Previsualizacion)
        thumbnailWidth: 154, //Medida de largo de la Previsualizacion
        thumbnailHeight: 154,//Medida alto Previsualizacion
        filesizeBase: 1000,
        maxFiles: 1,//define cuantos archivos se cargan al dropzone. Si se excede, se llamara al evento maxfilesexceeded.
        params: {}, //Parametros adicionales al formulario de envio ejemplo {tipo:"imagen"}
        clickable: true,
        ignoreHiddenFiles: true,
        acceptedFiles: "image/*", //extensiones
        acceptedMimeTypes: null,//Ya no se utiliza, paso a ser AceptedFiles
        autoProcessQueue: true,//True sube las imagenes automaticamente, si es false se tiene que llamar a myDropzone.processQueue(); para subirlas
        autoQueue: true,
        addRemoveLinks: true,//Habilita la posibilidad de eliminar/cancelar un archivo. Las opciones dictCancelUpload, dictCancelUploadConfirmation y dictRemoveFile se utilizan para la redacción.
        previewsContainer: null,//define dónde mostrar las previsualizaciones de archivos. Puede ser un HTMLElement liso o un selector de CSS. El elemento debe tener la estructura correcta para que las vistas previas se muestran correctamente.
        capture: null,
        dictDefaultMessage: "Arrastra los archivos aqui para subirlos",//missatges errors
        dictFallbackMessage: "Su navegador no soporta arrastrar y soltar para subir archivos.",
        dictFallbackText: "Por favor utilize el formuario de reserva de abajo como en los viejos tiempos.",
        dictFileTooBig: "La imagen supera el tamaño permitido ({{filesize}}MiB). Tam. Max : {{maxFilesize}}MiB. No se subira a nuestro servidor",
        dictInvalidFileType: "No se puede subir este tipo de archivos.",
        dictResponseError: "Server responded with {{statusCode}} code.",
        dictCancelUpload: "Cancel subida",
        dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
        dictRemoveFile: "Eliminar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos. No se subira a nuestro servidor",
        success: function(file, message){
            alert(file.status);
            if (file.status=="success") {
                $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente a nuestra base de datos.');
            }

        },
        removedfile: function(file) {
            //alert(file);
            var _ref;
            if (file.previewElement) {
              if ((_ref = file.previewElement) != null) {
                _ref.parentNode.removeChild(file.previewElement);
                var name = file.name;
                $.ajax({
                  type: "POST",
                  url: "../profile/delete/",
                  data: "filename="+name,
                  success:function(){
                    $('.msg').text('');
                    $('.msg').text('El archivo '+name+' se ha eliminado satisfactoriamente');

                  }
                });
                
              }
            }
            return this._updateMaxFilesReachedClass();
        },
        error: function(file,message) {
            //alert(file.status);
            //adecuar be els errors que venen de php
            alert(message);
            amaran(message);

        }
    });
    //////////// update client ////////////////
    $(document).on('click','#update_ubication',function(){
        combosDepenents("community_update","province_update","city_update");
        $("#update_ubication").css('display','none');
            
    });

        $(document).on('click','#Client_update',function(){
            //alert("entra en update");
            var id_community;
            var rdo = true;
            //que la fetxa no siga major a 2018
            var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
            var dnireg = /^[0-9]{8}[A-Z]{1}$/;
            var birthdayreg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var expdnireg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var last_namereg = /^[A-Za-z\sñÑ]{4,35}$/;
            var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
            var phonereg = /^[0-9]{9}$/;
                
            var dni = $("#dni_update").val();
            var name = $("#name_update").val();
            var last_name = $("#last_name_update").val();
            var birthday = $("#birthday_update").val();
            var exp_dni = $("#expirationDni_update").val();
            id_community = $("#community_update").val();
            
            if (!isNaN(id_community)) {
                var community = $("option[value="+id_community+"]").attr('id');
                
            }else{
                var community = id_community;

            }

            var id_province= $("#province_update").val();
            if (!isNaN(id_province)) {
                var province = $("option[value="+id_province+"]").attr('id');
            }else{
                var province = id_province;

            }
            
            var id_city = $("#city_update").val();
            if (!isNaN(id_city)) {
                var city = $("option[value="+id_city+"]").attr('id');
            }else{
                var city = id_city;

            }   
            
            let language = [];
            var inputElements = document.getElementsByClassName('checkboxs');
            var j = 0;
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    language[j] = inputElements[i].value;
                    j++;
                }
            }

            var email = $("#email_update").val();
            var phone = $("#phone_update").val();
            var type = "Client";
            
            /*var g = $('input[type="checkbox"]').button() var m = $('input[type="checkbox"]').val();       
            var l = $('#checkbox-4').prop('checked', true).button("refresh");*///per al UPDATE!!
            
            if( $("#dni_update").val() == ""){
                var message = "Introduce el dni";
                amaran(message);
                rdo=false;
                return false;
            }else if (!dnireg.test($("#dni_update").val())){
                var message = "El dni tiene que tener 8 numeros y una letra";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#name_update").val() == ""){
                var message = "Introduce el nombre";
                amaran(message);
                rdo=false;
                return false;
            }else if (!namereg.test($("#name_update").val())){
                var message = "El nombre tiene que tener entre 4-35 caracteres";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#last_name_update").val() == ""){
                var message = "Introduce apellidos";
                amaran(message);
                rdo=false;
                return false;
            }else if (!last_namereg.test($("#last_name_update").val())){
                var message = "Los apellidos tiene que tener entre 4-35 caracteres";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#birthday_update").val() == ""){
                var message = "Introduce la fecha";
                amaran(message);
                rdo=false;
                return false;
            }else if (!birthdayreg.test($("#birthday_update").val())){
                var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#expirationDni_update").val() == ""){
                var message = "Introduce la fecha de expiracion dni";
                amaran(message);
                rdo=false;
                return false;
            }else if (!expdnireg.test($("#expirationDni_update").val())){
                var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#email_update").val() == ""){
                var message = "Introduce su email";
                amaran(message);
                rdo=false;
                return false;
            }else if (!emailreg.test($("#email_update").val())){
                var message = "Tiene que ser un email valido. ejemplo@gmail.com";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#phone_update").val() == ""){
                var message = "Introduce su telefono";
                amaran(message);
                rdo=false;
                return false;
            }else if (!phonereg.test($("#phone_update").val())){
                var message = "El telefono tiene que tener 9 numeros";
                amaran(message);
                rdo=false;
                return false;
            }if($("#community_update").val() === "" || $("#community_update").val() === "Select community" || $("#community_update").val() === null) {
                var message = "Select one community";
                amaran(message);
                rdo=false;
                return false;//falta validate en PHP
            }if ($("#province_update").val() === "" || $("#province_update").val() === "Select province") {
                var message = "Select one province";
                amaran(message);
                rdo=false;
                return false;
            }
            if ($("#city_update").val() === "" || $("#city_update").val() === "Select city") {
                var message = "Select one city";
                amaran(message);
                rdo=false;
                return false;
            }
            if( $('input[name="checkboxs"]:checked').val() == undefined){
                var message = "Selecciona uno o varios idiomas";
                amaran(message);
                rdo=false;
                return false;
            }

            if (rdo) {

                var id = localStorage.getItem("id");
                //console.log("el rdo es true en client js");
                var data = { "id":id, "dni":dni, "name":name, "last_name":last_name, "birthday":birthday,"exp_dni":exp_dni, "community":community,"province":province,"city":city, "language":language, "email":email, "phone":phone , "type":type};
            
                var data_profile_JSON = JSON.stringify(data);
                console.log("json debug: "+data_profile_JSON);
    
                $.post(amigable("?module=profile&function=update_profile_client"),{alta_profile_json: data_profile_JSON},function (response){
                    alert(response);
                    var json = JSON.parse(response);
                    //alert(json.success);
                    console.log("success: "+json.success);
                    if (json.success) {
                        localStorage.getItem("update",true);
                        window.location.href = json.redirect;
                        amaran(json.msg);
                    }
                    
                   }).fail(function(xhr){
                    alert(xhr.responseText);
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                        //alert(xhr.responseJSON);
                        if (xhr.responseJSON.msg){
                            amaran(xhr.responseJSON.msg);

                        }

                        if (xhr.responseJSON.error.dni){
                            amaran(xhr.responseJSON.error.dni);
                            
                        }

                        if (xhr.responseJSON.error.validaDni){
                            amaran(xhr.responseJSON.error.validaDni);
                            
                        }

                        if (xhr.responseJSON.error.name){
                            amaran(xhr.responseJSON.error.name);
                         
                        }
                        if (xhr.responseJSON.error.last_name){
                            amaran(xhr.responseJSON.error.last_name);
                            
                        }

                        if (xhr.responseJSON.error.birthday){
                            amaran(xhr.responseJSON.error.birthday);
                            
                        }

                        if (xhr.responseJSON.error.exp_dni){
                            amaran(xhr.responseJSON.error.exp_dni);
                            
                        }

                        if (xhr.responseJSON.error.errorCompare){
                            amaran(xhr.responseJSON.error.errorCompare);
                            
                        }

                        if (xhr.responseJSON.error.language){
                            amaran(xhr.responseJSON.error.language);
                            
                        }

                        if (xhr.responseJSON.error.email){
                            amaran(xhr.responseJSON.error.email);
                            
                        }

                        if (xhr.responseJSON.error.phone){
                            amaran(xhr.responseJSON.error.phone);
                            
                        }

                        if (xhr.responseJSON.error.country){
                            amaran(xhr.responseJSON.error.country);
                            
                        }
                        if(xhr.responseJSON.error.province){
                            amaran(xhr.responseJSON.error.province);

                        }

                        if(xhr.responseJSON.error.city){
                            amaran(xhr.responseJSON.error.city);

                        }

               });//end fail

            }//end rdo

            
        });

    //////////// update freelancer ////////////////
    $(document).on('click','#update_ubication_free',function(){
        combosDepenents("community_free_update","province_free_update","city_free_update");
        $("#update_ubication").css('display','none');
            
    });

        $(document).on('click','#Freelancer_update',function(){
            //alert("entra en update");
            var id_community;
            var rdo = true;
            //que la fetxa no siga major a 2018
            var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
            var dnireg = /^[0-9]{8}[A-Z]{1}$/;
            var birthdayreg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var expdnireg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var last_namereg = /^[A-Za-z\sñÑ]{4,35}$/;
            var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
            var phonereg = /^[0-9]{9}$/;
                
            var dni = $("#dni_update").val();
            var name = $("#name_update").val();
            var last_name = $("#last_name_update").val();
            var birthday = $("#birthday_update").val();
            var exp_dni = $("#expirationDni_update").val();
            id_community = $("#community_update").val();
            
            if (!isNaN(id_community)) {
                var community = $("option[value="+id_community+"]").attr('id');
                
            }else{
                var community = id_community;

            }

            var id_province= $("#province_update").val();
            if (!isNaN(id_province)) {
                var province = $("option[value="+id_province+"]").attr('id');
            }else{
                var province = id_province;

            }
            
            var id_city = $("#city_update").val();
            if (!isNaN(id_city)) {
                var city = $("option[value="+id_city+"]").attr('id');
            }else{
                var city = id_city;

            }   
            
            let language = [];
            var inputElements = document.getElementsByClassName('checkboxs');
            var j = 0;
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    language[j] = inputElements[i].value;
                    j++;
                }
            }

            var email = $("#email_update").val();
            var phone = $("#phone_update").val();
            var type = "Client";
            
            /*var g = $('input[type="checkbox"]').button() var m = $('input[type="checkbox"]').val();       
            var l = $('#checkbox-4').prop('checked', true).button("refresh");*///per al UPDATE!!
            
            if( $("#dni_update").val() == ""){
                var message = "Introduce el dni";
                amaran(message);
                rdo=false;
                return false;
            }else if (!dnireg.test($("#dni_update").val())){
                var message = "El dni tiene que tener 8 numeros y una letra";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#name_update").val() == ""){
                var message = "Introduce el nombre";
                amaran(message);
                rdo=false;
                return false;
            }else if (!namereg.test($("#name_update").val())){
                var message = "El nombre tiene que tener entre 4-35 caracteres";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#last_name_update").val() == ""){
                var message = "Introduce apellidos";
                amaran(message);
                rdo=false;
                return false;
            }else if (!last_namereg.test($("#last_name_update").val())){
                var message = "Los apellidos tiene que tener entre 4-35 caracteres";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#birthday_update").val() == ""){
                var message = "Introduce la fecha";
                amaran(message);
                rdo=false;
                return false;
            }else if (!birthdayreg.test($("#birthday_update").val())){
                var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#expirationDni_update").val() == ""){
                var message = "Introduce la fecha de expiracion dni";
                amaran(message);
                rdo=false;
                return false;
            }else if (!expdnireg.test($("#expirationDni_update").val())){
                var message = "La fecha tiene que tener el siguente formato dd/mm/aaaa";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#email_update").val() == ""){
                var message = "Introduce su email";
                amaran(message);
                rdo=false;
                return false;
            }else if (!emailreg.test($("#email_update").val())){
                var message = "Tiene que ser un email valido. ejemplo@gmail.com";
                amaran(message);
                rdo=false;
                return false;
            }if( $("#phone_update").val() == ""){
                var message = "Introduce su telefono";
                amaran(message);
                rdo=false;
                return false;
            }else if (!phonereg.test($("#phone_update").val())){
                var message = "El telefono tiene que tener 9 numeros";
                amaran(message);
                rdo=false;
                return false;
            }if($("#community_update").val() === "" || $("#community_update").val() === "Select community" || $("#community_update").val() === null) {
                var message = "Select one community";
                amaran(message);
                rdo=false;
                return false;//falta validate en PHP
            }if ($("#province_update").val() === "" || $("#province_update").val() === "Select province") {
                var message = "Select one province";
                amaran(message);
                rdo=false;
                return false;
            }
            if ($("#city_update").val() === "" || $("#city_update").val() === "Select city") {
                var message = "Select one city";
                amaran(message);
                rdo=false;
                return false;
            }
            if( $('input[name="checkboxs"]:checked').val() == undefined){
                var message = "Selecciona uno o varios idiomas";
                amaran(message);
                rdo=false;
                return false;
            }

            if (rdo) {

                var id = localStorage.getItem("id");
                //console.log("el rdo es true en client js");
                var data = { "id":id, "dni":dni, "name":name, "last_name":last_name, "birthday":birthday,"exp_dni":exp_dni, "community":community,"province":province,"city":city, "language":language, "email":email, "phone":phone , "type":type};
            
                var data_profile_JSON = JSON.stringify(data);
                console.log("json debug: "+data_profile_JSON);
    
                $.post(amigable("?module=profile&function=update_profile_client"),{alta_profile_json: data_profile_JSON},function (response){
                    alert(response);
                    var json = JSON.parse(response);
                    //alert(json.success);
                    console.log("success: "+json.success);
                    if (json.success) {
                        localStorage.getItem("update",true);
                        window.location.href = json.redirect;
                        amaran(json.msg);
                    }
                    
                   }).fail(function(xhr){
                    alert(xhr.responseText);
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                        //alert(xhr.responseJSON);
                        if (xhr.responseJSON.msg){
                            amaran(xhr.responseJSON.msg);

                        }

                        if (xhr.responseJSON.error.dni){
                            amaran(xhr.responseJSON.error.dni);
                            
                        }

                        if (xhr.responseJSON.error.validaDni){
                            amaran(xhr.responseJSON.error.validaDni);
                            
                        }

                        if (xhr.responseJSON.error.name){
                            amaran(xhr.responseJSON.error.name);
                         
                        }
                        if (xhr.responseJSON.error.last_name){
                            amaran(xhr.responseJSON.error.last_name);
                            
                        }

                        if (xhr.responseJSON.error.birthday){
                            amaran(xhr.responseJSON.error.birthday);
                            
                        }

                        if (xhr.responseJSON.error.exp_dni){
                            amaran(xhr.responseJSON.error.exp_dni);
                            
                        }

                        if (xhr.responseJSON.error.errorCompare){
                            amaran(xhr.responseJSON.error.errorCompare);
                            
                        }

                        if (xhr.responseJSON.error.language){
                            amaran(xhr.responseJSON.error.language);
                            
                        }

                        if (xhr.responseJSON.error.email){
                            amaran(xhr.responseJSON.error.email);
                            
                        }

                        if (xhr.responseJSON.error.phone){
                            amaran(xhr.responseJSON.error.phone);
                            
                        }

                        if (xhr.responseJSON.error.country){
                            amaran(xhr.responseJSON.error.country);
                            
                        }
                        if(xhr.responseJSON.error.province){
                            amaran(xhr.responseJSON.error.province);

                        }

                        if(xhr.responseJSON.error.city){
                            amaran(xhr.responseJSON.error.city);

                        }

               });//end fail

            }//end rdo

            
        });
}
