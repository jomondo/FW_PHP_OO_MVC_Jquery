$(document).ready(function(){
	$(".ajaxLoader").css('display','none');
	
	$(document).on('click','.submitConsulting',function(){
        var rdo = true;
        var namereg = /^[A-Za-z\sñÑ]{4,20}$/;
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var msgreg = /^[A-Za-z\sñÑ0-9]{10,450}$/;

        var name = $('#name_contact').val();
        var email = $('#email_contact').val();
        var subject = $('#subject_contact').val();
        var mgs = $('#msg_contact').val();

        if( $("#name_contact").val() == ""){
            var message = "Introduce su nombre";
            amaran(message);
            rdo=false;
            return false;

        }else if (!namereg.test($("#name_contact").val())){
            var message = "El nombre tiene que incluir de 4-20 letras";
            amaran(message);
            rdo=false;
            return false;

        }if( $("#email_contact").val() == ""){
            var message = "Introduce su email";
            amaran(message);
            rdo=false;
            return false;

        }else if (!emailreg.test($("#email_contact").val())){
            var message = "Tiene que introducir un email valido";
            amaran(message);
            rdo=false;
            return false;

        }if( $("#subject_contact").val() == ""){
            var message = "Seleccione un tema";
            amaran(message);
            rdo=false;
            return false;

        }if( $("#msg_contact").val() == ""){
            var message = "Introduce su consulta";
            amaran(message);
            rdo=false;
            return false;

        }else if (!msgreg.test($("#msg_contact").val())){
            var message = "La consulta permite entre 10-450 caracteres";
            amaran(message);
            rdo=false;
            return false;

        }

        if (rdo) {
            var data = { "name":name, "email":email, "subject":subject, "mgs":mgs};
            var data_val_JSON = JSON.stringify(data);
            console.log("json debug data_contact: " + data_val_JSON);
            $(".ajaxLoader").css('display','block');
            $('.ajaxLoader').fadeIn("fast");

            $.post('../contact/process_consulting/',{data: data_val_JSON},function (response){
                //alert(response);
                $('.ajaxLoader').fadeOut("fast");
                $(".ajaxLoader").css('display','none');
                alert("Verifique en su correo que se ha entregado correctamente.");
                $('#name_contact').val('');
		        $('#email_contact').val('');
		        $('#subject_contact').val('');
		        $('#msg_contact').val('');
                
            }).fail(function(xhr){
                alert(xhr.responseText);    

            });

        }
        
    });//end click

});