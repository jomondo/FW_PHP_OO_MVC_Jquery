<?php
	class controller_contact{
		function __construct(){
				
		}

		function begin(){
			loadView("contact","contact");
		}

		function process_consulting(){
			if (isset($_POST['data'])) {
				//echo json_encode($_POST['data']);
				//exit;
				$dataJSON = json_decode($_POST["data"], true);
				/* envio email user */
				$arrValues = array(
					'type' => 'contact',
					'name' => $dataJSON['name'],
					'email' => $dataJSON['email'],
					'subject' => $dataJSON['subject'],
					'mgs' => $dataJSON['mgs']
				);

				try{
                	send_email($arrValues);

				}catch (Exception $e) {
					showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);

				}

				/* envio email admin */
				$arrValues = array(
					'type' => 'admin',
					'name' => $dataJSON['name'],
					'email' => $dataJSON['email'],
					'subject' => $dataJSON['subject'],
					'mgs' => $dataJSON['mgs']
				);

				try{
					sleep(5);
                	send_email($arrValues);

				}catch (Exception $e) {
					showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);

				}

			}else{
				showErrorPage(0, "ERROR - 400 not found data", 'HTTP/1.0 400 Bad Request', 400);

			}
		}

	}