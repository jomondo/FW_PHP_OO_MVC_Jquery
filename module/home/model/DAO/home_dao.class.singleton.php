<?php
class home_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_freelancer_DAO($db) {
        //print_r($db);
        //exit;
        $sql = "SELECT * FROM autonomo";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }

    public function obtain_price_h_DAO($db) {
        //print_r("entra en obtain_price_h_DAO");
        //exit;
        $sql = "SELECT DISTINCT price_h FROM autonomo";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    
    }
    
    public function details_freelancer_DAO($db,$id) {
        $sql = "SELECT * FROM autonomo WHERE dni='".$id."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_client_DAO($db) {
        $sql = "SELECT count(*) as count FROM cliente";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_free_DAO($db) {
        $sql = "SELECT count(*) as count FROM autonomo";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function pagination_freelancer_DAO($arrayValues,$db) {
        $pos = $arrayValues['position'];
        $user_per_page = $arrayValues['user_per_page'];
        $name_categ = $arrayValues['category'];
        
        $sql = "SELECT * FROM autonomo WHERE categ_prof='".$name_categ."' LIMIT ".$pos." , ".$user_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_by_category_DAO($id_category,$db) {

        $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof='".$id_category['categ_prof']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_val_category_DAO($id_category,$db) {

        $sql = "SELECT value,name_catg FROM category WHERE name_catg='".$id_category['categ_prof']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function update_val_category_DAO($val,$db) {

        $sql = "UPDATE category SET value='".$val['val_categ']."' WHERE name_catg='".$val['categ_prof']."'";

        return $stmt = $db->ejecutar($sql);

    }

    public function get_category_DAO($db) {

        //$sql = "SELECT DISTINCT categ_prof FROM autonomo";
        $sql = "SELECT name_catg as categ_prof FROM category ORDER BY category.value DESC LIMIT 4";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_category_DAO($db) {

        //$sql = "SELECT DISTINCT categ_prof FROM autonomo";
        $sql = "SELECT name_catg FROM category";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_especialist_DAO($db) {

        $sql = "SELECT DISTINCT especialista FROM autonomo";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function obtain_province_DAO($db) {

        $sql = "SELECT DISTINCT province FROM autonomo";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function paintAutocomplete_DAO($db) {

        $sql = "SELECT DISTINCT especialista as label, categ_prof as category FROM autonomo";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function dataSearch_DAO($arrayValues,$db) {
        $especialista = $arrayValues['especialista'];
        $name_categ = $arrayValues['categ_prof'];
        
        $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof='".$name_categ."' and especialista='".$especialista."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function list_Search_DAO($arrayValues,$db) {
        $pos = $arrayValues['position'];
        $especialista = $arrayValues['especialista'];
        $name_categ = $arrayValues['categ_prof'];
        $user_per_page = $arrayValues['user_per_page'];

        $sql = "SELECT * FROM autonomo WHERE categ_prof='".$name_categ."' and especialista='".$especialista."' LIMIT ".$pos." , ".$user_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_searchKey_DAO($arrayValues,$db) {

        $sql = "SELECT count(*) as count FROM autonomo WHERE especialista LIKE '%".$arrayValues['name_key']."%' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function list_KeySearch_DAO($arrayValues,$db) {
        $pos = $arrayValues['position'];
        $name_key = $arrayValues['name_key'];
        $user_per_page = $arrayValues['user_per_page'];
        
        //echo json_encode($name_key);
        //exit;
        $sql = "SELECT * FROM autonomo WHERE especialista LIKE '%".$name_key."%' LIMIT ".$pos." , ".$user_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function apply_filters_DAO($arrayValues,$db) {

        if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['province']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and province = '".$arrayValues['province']."' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' ";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['province'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and province = '".$arrayValues['province']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['name_key']) && !empty($arrayValues['province'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%' and province = '".$arrayValues['province']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['province']) && !empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE province = '".$arrayValues['province']."' and especialista LIKE '%".$arrayValues['name_key']."%' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof']) && !empty($arrayValues['province'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and province = '".$arrayValues['province']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['especialista']) && !empty($arrayValues['province'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE especialista = '".$arrayValues['especialista']."' and province = '".$arrayValues['province']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['province'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and province = '".$arrayValues['province']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['especialista'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and especialista = '".$arrayValues['especialista']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['categ_prof'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and categ_prof = '".$arrayValues['categ_prof']."'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['categ_prof'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['province'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE province = '".$arrayValues['province']."' and especialista LIKE '%".$arrayValues['name_key']."%'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and especialista LIKE '%".$arrayValues['name_key']."%'";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof'])){
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' ";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['especialista'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE especialista = '".$arrayValues['especialista']."' ";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['province'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE province = '".$arrayValues['province']."' ";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])){
            //echo '<script language="javascript">console.log("Entra en price_h")</script>';
            $sql = "SELECT count(*) as count FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' ";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }


        

    }

    public function list_filter_DAO($arrayValues,$db) {
        $pos = $arrayValues['position'];
        $categ_prof = $arrayValues['categ_prof'];
        $especialista = $arrayValues['especialista'];
        $province = $arrayValues['province'];
        $price_h = $arrayValues['price_h'];
        $user_per_page = $arrayValues['user_per_page'];
        
        //echo json_encode($arrayValues);
        //exit;
        

        if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['province']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '1';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and province = '".$arrayValues['province']."' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['province'])) {
            //echo '2';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and province = '".$arrayValues['province']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
            //echo '3';
            //print_r($arrayValues['price_max']);
            //exit;
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista = '".$arrayValues['especialista']."' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['name_key']) && !empty($arrayValues['province'])) {
            //echo '4';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%' and province = '".$arrayValues['province']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['categ_prof']) && !empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if (!empty($arrayValues['province']) && !empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])) {
           // echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE province = '".$arrayValues['province']."' and especialista LIKE '%".$arrayValues['name_key']."%' and price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' LIMIT ".$pos." , ".$user_per_page."";

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof']) && !empty($arrayValues['especialista'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$categ_prof."' and especialista = '".$arrayValues['especialista']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof']) && !empty($arrayValues['province'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$categ_prof."' and province = '".$arrayValues['province']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['especialista']) && !empty($arrayValues['province'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE especialista = '".$especialista."' and province = '".$arrayValues['province']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['province'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and province = '".$arrayValues['province']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['especialista'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and especialista = '".$arrayValues['especialista']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min']) && !empty($arrayValues['categ_prof'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and categ_prof = '".$arrayValues['categ_prof']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['categ_prof'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$arrayValues['categ_prof']."' and especialista LIKE '%".$arrayValues['name_key']."%' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' and especialista LIKE '%".$arrayValues['name_key']."%' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['name_key']) && !empty($arrayValues['province'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE province = '".$arrayValues['province']."' and especialista LIKE '%".$arrayValues['name_key']."%' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['categ_prof'])){
            //echo '<script language="javascript">console.log("Entra en list try")</script>';
            $sql = "SELECT * FROM autonomo WHERE categ_prof = '".$categ_prof."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['especialista'])){

            $sql = "SELECT * FROM autonomo WHERE especialista = '".$especialista."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);
            
        }else if(!empty($arrayValues['province'])){
            $sql = "SELECT * FROM autonomo WHERE province = '".$province."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }else if(!empty($arrayValues['price_max']) && !empty($arrayValues['price_min'])){
        //echo '<script language="javascript">console.log("Entra en price_h list")</script>';

            $sql = "SELECT * FROM autonomo WHERE price_h >= '".$arrayValues['price_min']."' and price_h <= '".$arrayValues['price_max']."' LIMIT ".$pos." , ".$user_per_page;

            $stmt = $db->ejecutar($sql);
            return $db->listar($stmt);

        }

    }

    public function submitValueWeb_DAO($arrayValues,$db) {
        
        $sql = "INSERT INTO value_website ( name, stars, message, avatar ) VALUES ('$arrayValues[name]', '$arrayValues[stars]','$arrayValues[mgs]', '$arrayValues[avatar]')";

        return $stmt = $db->ejecutar($sql);

    }

    public function insert_best_freelancer_DAO($arrayValues,$db) {
        
        $sql = "INSERT INTO best_freelancer ( dni_freelancer,stars ) VALUES ('$arrayValues[dni_valorado]', '$arrayValues[stars]')";

        return $stmt = $db->ejecutar($sql);

    }

    public function update_stars_DAO($arrayValues,$db) {
        
        $sql = "UPDATE best_freelancer SET stars = '".$arrayValues['stars']."' WHERE dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        return $stmt = $db->ejecutar($sql);

    }

    public function verify_rating_DAO($arrayValues,$db) {
        
        $sql = "SELECT count(*) as count FROM value_freelancer WHERE id_valorador = '".$arrayValues['id_valorador']."' and dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function count_best_freelancer_DAO($arrayValues,$db) {
        
        $sql = "SELECT count(*) as count FROM best_freelancer WHERE dni_freelancer = '".$arrayValues['dni_valorado']."' ";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function select_stars_DAO($dni,$db) {
        
        $sql = "SELECT stars FROM value_freelancer WHERE dni_freelancer = '".$dni."' ORDER BY stars DESC LIMIT 1";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function submitValueFreelancer_DAO($arrayValues,$db) {
    
        $sql = "INSERT INTO value_freelancer ( dni_freelancer, id_valorador, stars ) VALUES ('$arrayValues[dni_valorado]', '$arrayValues[id_valorador]','$arrayValues[stars]')";

        return $db->ejecutar($sql);

    }

    public function best_clients_DAO($db) {

        $sql = "SELECT * FROM value_website ORDER BY value_website.stars DESC LIMIT 3";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_best_freelancer_DAO($db) {

        $sql = "SELECT * FROM best_freelancer ORDER BY stars DESC LIMIT 6";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_data_freelancer_DAO($db,$arrayValues) {

        $sql = "SELECT * FROM autonomo WHERE dni = '".$arrayValues['dni1']."' or dni = '".$arrayValues['dni2']."' or dni = '".$arrayValues['dni3']."' or dni = '".$arrayValues['dni4']."' or dni = '".$arrayValues['dni5']."' or dni = '".$arrayValues['dni6']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function get_markets_DAO($db,$arrayValues) {

        $sql = "SELECT * FROM city_coordenadas WHERE city = '".$arrayValues['city']."' and dni = '".$arrayValues['dni']."'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

    public function data_geolocation_DAO($db,$arrayValues) {

        $sql = "SELECT name, categ_prof, especialista, materia, price_h FROM city_coordenadas INNER JOIN autonomo ON city_coordenadas.dni = autonomo.dni WHERE autonomo.dni = '".$arrayValues['dni']."';";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

}
