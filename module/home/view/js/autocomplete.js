$(document).ready(function(){
  //alert("entra en autocomplete");
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
        _create: function() {
          this._super();
          this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        },
        _renderMenu: function( ul, items ) {
          var that = this,
            currentCategory = "";
          $.each( items, function( index, item ) {
            var li;
            if ( item.category != currentCategory ) {
              ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
              currentCategory = item.category;
            }
            li = that._renderItemData( ul, item );
            if ( item.category ) {
              li.attr( "aria-label", item.category + " : " + item.label );
            }
          });
        }
      });
      //module/home/controller/controller_home.class.php?paintAutocomplete=true

      var pag = localStorage.getItem("page");
      //alert(pag);
      if (pag=="listUser") {
        //alert("entra en listar user home");
        var url1 = "../home/getAutocomplete/";
        var url2 = "../home/search/";
        var url3 = "../home/countSearchKey/";
        
        localStorage.setItem("page_home","home");
        localStorage.removeItem("page");

      }else{
        //alert("entra en auto_list");
        localStorage.removeItem("page_home");
        localStorage.setItem("auto_list","auto_listar");

        var url1 = "../../home/getAutocomplete/";
        var url2 = "../../home/search/";
        var url3 = "../../home/countSearchKey/";

      }
      //alert("auto: "+url1);
      $.ajax({
          url: url1,
          type: 'GET',
          success:function (response) {
              //alert(response);
              var json = JSON.parse(response);
              $( "#searchHome" ).catcomplete({
                  delay: 0,
                  source: json,
                  select: function(event, ui) {
                      var especialista = ui.item.label;
                      var categ_prof = ui.item.category;
                      var data = {"especialista":especialista,"categ_prof":categ_prof};
                      var search_JSON = JSON.stringify(data);
                      //alert(search_JSON);
                      load_data_ajax(url2,"POST",search_JSON,"",1);

                  },
              });
                      
          },
          error:function(xhr){
              alert(xhr.responseText);
          }

      });

      $(document).on('click','#btn_search',function(){
          var rdo = validaSearchHome();
          if (rdo===true) {
              var search = $("#searchHome").val();
              localStorage.setItem("search_filter",search);
              countSearch(search);
          }

      });


      function validaSearchHome(){
        var searchreg = /^[A-Za-z\sñÑ]{0,25}$/;
        var search = $("#searchHome").val();

        if (!searchreg.test($("#searchHome").val())){
            var message = "Entre 0 y 25 letras";
            amaran(message);
            return false;

        }

        return true;

      }

    function countSearch(search){
      var data = {"name_key":search};
      console.log(data);
      var data_json = JSON.stringify(data);
      //"module/home/controller/controller_home.class.php?countSearchKey=true
      load_data_ajax(url3,"POST",data_json,"",1);
        
    }
});