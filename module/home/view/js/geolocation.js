function geolocation (url,value_name,make){

  if (make == "staticMap") {
    var value = JSON.stringify(value_name);
          
    $.ajax({
      url: url,
      type: "POST",
      data:{data:value},
      success:function (response) {
        //alert(response);
        var markers = JSON.parse(response);

        var map = new google.maps.Map(document.getElementById("map"), {
          center: new google.maps.LatLng(markers.value[0].lat,markers.value[0].lng ),
          zoom: 15,
          mapTypeId: 'roadmap'

        });

        var point = new google.maps.LatLng(parseFloat(markers.value[0].lat),parseFloat(markers.value[0].lng));
        var marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: point,

        });

        marker.setAnimation(google.maps.Animation.BOUNCE);
                  
      },
      error:function(xhr){
        alert(xhr.responseText);
      }

    });
            
  }else if (make == "list_maps") {
    var j = [];
    var MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';

    // reiniciar results markers
    $('tbody#results').remove();
    $('div#tabla').append('<tbody id="results"></tbody>');

    value_name.forEach(function(elementos){
      var data = {"city":elementos.city, "dni":elementos.dni};
      var value = JSON.stringify(data);
      console.log("debig value_name: "+value);

      $.ajax({
        url: url,
        type: "POST",
        data:{data:value},
        success:function (response) {
          //alert(response);
          var json = JSON.parse(response);
          j.push(json);
          console.log(j);
          //alert(j[1].value[0].city);
          var infoWindow = new google.maps.InfoWindow;
          var map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(40.4167754,-3.7037901999999576 ),
            zoom: 5,
            mapTypeId: 'roadmap'

          });

          var cont = 0;
          var datos = [];
          j.forEach(function(elemento){
            //for (var i = 0; i < j.length; i++) {
            //alert(cont);
            //alert("dni que mana: " + elemento.value[0].dni);
            var data = {"dni":elemento.value[0].dni};
            var data_JSON = JSON.stringify(data);
            $.ajax({
              url:'../../home/data_geolocation/',
              type: "POST",
              data:{data:data_JSON},
              success:function (response) {
                //alert(response);
                var json = JSON.parse(response);
                var name = elemento.value[0].city;
      
                var markerLetter = String.fromCharCode('A'.charCodeAt(0) + cont);
                var markerIcon = MARKER_PATH + markerLetter + '.png';
                var point = new google.maps.LatLng(parseFloat(elemento.value[0].lat),parseFloat(elemento.value[0].lng));
                var html = "<b>" + json.value[0].categ_prof + "</b> <br/>" + "<p>" + json.value[0].especialista + "</p>" + "<p>" + json.value[0].price_h + "€/h</p> <br/>" + name;
                //var icon = customIcons[type] || {};
                var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  position: point,
                  icon: markerIcon

                });

                addResult(name,cont);
                cont++;
                bindInfoWindow(marker, map, infoWindow, html);

                //////////// functions /////////////////////////

                function bindInfoWindow(marker, map, infoWindow, html) {
                  google.maps.event.addListener(marker, 'click', function() {
                    infoWindow.setContent(html);
                    infoWindow.open(map, marker);

                  });

                }
                
                function addResult(result, i) {  
                  var markerLetter = String.fromCharCode('A'.charCodeAt(0) + i);
                  var markerIcon = MARKER_PATH + markerLetter + '.png';

                  var tr = document.createElement('tr');
                  //$("tbody#results").append("<tr id='col_city'><td><img class='placeIcon' src='"+markerIcon+"'></img></td><td>"+result+"</td></tr>");
                  tr.style.backgroundColor = (i % 2 === 0 ? '#F0F0F0' : '#FFFFFF');
                  tr.onclick = function() {
                    //markers[i]
                    google.maps.event.trigger(marker, 'click');

                  };

                  var iconTd = document.createElement('td');
                  var nameTd = document.createElement('td');
                  var icon = document.createElement('img');
                  icon.src = markerIcon;
                  icon.setAttribute('class', 'placeIcon');
                  icon.setAttribute('className', 'placeIcon');
                  tr.setAttribute('class', 'col_city');
                  var name = document.createTextNode(result);
                  iconTd.appendChild(icon);
                  nameTd.appendChild(name);
                  tr.appendChild(iconTd);
                  tr.appendChild(nameTd);
                  results.appendChild(tr);
                  
                  var w = $("tr.col_city").length;
                  if (w==1) {
                    $("tr.col_city").remove();
                    $("tbody#results").append("<tr class='col_city'></tr>");

                  }
                  
                }
              },
              error:function(xhr){
                alert(xhr.responseText);
              }

            });
   
          });//end foreach

        },
        error:function(xhr){
          alert(xhr.responseText);

        }

      });
    });
         
  }//else if
            
}//function