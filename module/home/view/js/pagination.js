//alert("entra en pagination");
function pagination (url1,url2,name_list, name_pag){
	$.ajax({
		url: '../../home/'+url1+'/',
		type: 'GET',
		success:function(response){
            //alert(response);
 			var json = JSON.parse(response);
            //alert(json.count);
 			var count = json.count;
 			var name = json.data;
            var especialista = json.data.especialista;
            var categ_prof = json.data.categ_prof;
            var province = json.data.province;
            var price_h = json.data.price_h;
            var price_min = json.data.price_min;
            var price_max = json.data.price_max;
            
 			if (count==0) {
                var msg = "Lo sentimos, no hay ningun resultado";
                amaran(msg);
 				return false;

 			}
            if (name_pag=="pag_filter") {
               $("div#paginations").remove();
               $("div#pag_filter").remove(); 
               $("div#pagination_filter").append('<div id="pag_filter"></div>');
               
            }
            
 			$("#"+name_pag).bootpag({
                total: count,
                page: 1,
                maxVisible: 2,
                next: '>',
                prev: '<'
            }).on("page", function (e, num) {
                //alert(num);
                var data={"num_page":num, "name":name, "especialista":especialista,"categ_prof":categ_prof,"province":province, "price_min":price_min, "price_max":price_max};
                var num_page = JSON.stringify(data);
                load_data_ajax("../../home/"+url2+"/","POST",num_page,name_list,"list_page");
                
            });
            
            /*--- first list -----*/
            var num = 1;
            var data={"num_page":num,"name":name, "especialista":especialista,"categ_prof":categ_prof, "province":province, "price_min":price_min, "price_max":price_max};
            console.log(data);
            var num_page = JSON.stringify(data);
            load_data_ajax("../../home/"+url2+"/","POST",num_page,name_list,"list_page");
 			
		},
		error:function(xhr){
			alert(xhr.responseText);
            //alert("ERROR");
			if (xhr.responseText.error) {
				amaran(xhr.responseText.error);

			}
			
		},


	});

	//clavaro en utils del modul home
	$(document).on('click','.buttonDetails',function(){
        var id = this.getAttribute('id');
        var data = {"dni":id};
        var dni_JSON = JSON.stringify(data);
        load_data_ajax("../../home/details_list/","POST",dni_JSON,"","details");

    });


}   