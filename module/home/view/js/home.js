$(document).ready(function(){
    localStorage.setItem("page","listUser");

    paint_best_freelancer();
	//paintUser();
    paintDetailsUser();
	paintStatistic();
    paintBestClientHome();
    validate_rating_stars();

});

function paint_best_freelancer(){
    load_data_ajax("../home/get_best_freelancer/","GET","paintbestFreelancer","","paintHome");

}

function paintDetailsUser(){
    $(document).on('click','.buttonDetails',function(){
        var id = this.getAttribute('id');
        var data = {"dni":id};
        var dni_JSON = JSON.stringify(data);
        load_data_ajax("../home/details/","POST",dni_JSON,"","details");
        
    });
}

function paintStatistic (){
    load_data_ajax("../home/countStatisticCli/","GET","paintStatisticHome","countClient","paintHome");
    load_data_ajax("../home/countStatisticFree/","GET","paintStatisticHome","countFreelancer","paintHome");
        
}

function paintBestClientHome(){
    //alert("entra en js paintBestClient");
    load_data_ajax("../home/paintBestClient/","GET","paintBestClientHome","","paintHome");

}

function validate_rating_stars(){
    $(document).on('click','.submitStars',function(){
        var rdo = true;
        
        var msgreg = /^[A-Za-z\sñÑ0-9]{10,150}$/;
        //var starsreg = /^([0-9]{1}[.]{1}[0-9]{1}|[0-9]{1})$/;

        var stars = $('#value_stars').val();
        var mgs = $('#message_stars').val();

        if( $("#value_stars").val() == ""){
            var message = "Seleccione su puntuacion con las estrellas. Minima puntuacion 0.1";
            amaran(message);
            rdo=false;
            return false;

        }if( $("#message_stars").val() == ""){
            var message = "Introduce su opinion de nuestra web";
            amaran(message);
            rdo=false;
            return false;

        }else if (!msgreg.test($("#message_stars").val())){
            var message = "La opinion permite entre 10-150 caracteres";
            amaran(message);
            rdo=false;
            return false;

        }

        if (rdo) {
            var data = localStorage.getItem("datos_user");
            var json = JSON.parse(data);
            if (data!==null) {
                if (json.token) {
                    var tokenInit = {"token":json.token};
                    var tokenToInit = JSON.stringify(tokenInit);
                    var data = {"mgs":mgs,"stars":stars};
                    var data_rating = JSON.stringify(data);
                    
                    load_data_ajax("?module=login&function=select_user_token","POST",tokenToInit,data_rating,"select_user_rating");
                    
                }
                
            }

        }//end rdo
        
    });//end click
}

