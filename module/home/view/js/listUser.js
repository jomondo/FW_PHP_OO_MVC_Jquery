$(document).ready(function() {
	/*---- obtener num de visitas y nombre categ y hacer update------*/
	var val_categ = localStorage.getItem("data_categ");
	//alert("pros: " + val_categ);
	if (val_categ!==null) {
		load_data_ajax("../../home/updateValCateg/","POST",val_categ,"",3);

	}
	var redirect = localStorage.getItem("redirect");
	//alert(redirect);
	//alert("entra en general listUser");
    if (redirect=="listUser") {
    	//alert("entra en listUser");
    	var url1 = "paintListFreelancer";
	    var url2 = "num_page";
	    var name_list = "listar";
	    var name_pag = "paginations";

	    pagination(url1, url2, name_list, name_pag);


    }else if (redirect=="listSearchHome") {
    	//alert("entra en listSearchHome");
    	var url1 = "countListSearch";
	    var url2 = "paintListSearch";
	    var name_list = "listar";
	    var name_pag = "paginations";

	    pagination(url1, url2, name_list, name_pag);

    }else if (redirect=="listKeySearch") {
    	var url1 = "ListSearchKey";
	    var url2 = "paintListKeySearch";
	    var name_list = "listar";
	    var name_pag = "paginations";

	    pagination(url1, url2, name_list, name_pag);
    	
    }

	filters();

});

function filters(){
	//llenar filters
	load_data_ajax("../../home/obtainCateg/","GET","","","filters");
	load_data_ajax("../../home/obtainEspecialist/","GET","","","filters");
	load_data_ajax("../../home/obtainProvince/","GET","","","filters");

    $( "#filter_especial" ).selectmenu({width: 200});
    $( "#filter_especialista" ).selectmenu({width: 200});
    $( "#filter_province" ).selectmenu({width: 200});

    $(document).on('click','.submitFilter',function(){
    	var category = $("#filter_especial").val();
    	var especialista = $("#filter_especialista").val();
    	var province = $("#filter_province").val();
    	var price_h = $("#filter_price_h").val();
        var redirect = localStorage.getItem("redirect");

    	if (category=="" && especialista=="" && province=="" && price_h=="") {
    		var msg = "No ha seleccionado ningun filtro";
    		amaran(msg);
    		return false;
    	}

        if (redirect=="listUser" && category=="" ) {
            var category = $(".list_cat").attr('id');
            //alert(category);

        }else if(redirect=="listSearchHome" && especialista=="" ){
            var especialista = $(".list_esp").attr('id');
           // alert(especialista);

        }else if(redirect=="listKeySearch" ){
            var search = localStorage.getItem("search_filter");
            //alert(search);
            if (especialista!="") {
                search = "";

            }

        }
        var price_min = localStorage.getItem("slider_price_min");
        var price_max = localStorage.getItem("slider_price_max");
        
        var data = {"categ_prof":category,"especialista":especialista,"province":province, "price_min":price_min,"price_max":price_max, "name_key":search};
        console.log(data);
        //return false;
      	var data_JSON = JSON.stringify(data);
      	console.log(data_JSON);
        load_data_ajax("../../home/apply_filters/","POST",data_JSON,"","obtain_filters");

   	});

    ///////////////////// price min and max //////////////////////////////////
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 450,
        values: [ 10, 80 ],
        slide: function( event, ui ){
            //alert(ui.values[0]);
            $( "#amount" ).val( "Price: " + ui.values[0] + "€" + " - " + ui.values[1] + "€");
                
            localStorage.setItem("slider_price_min",ui.values[0]);
            localStorage.setItem("slider_price_max",ui.values[1]);
            /*var data_price_Json = JSON.stringify(data);*/
                
            
        }
    });
    $( "#amount" ).val(  "Price: " + $( "#slider-range" ).slider( "values", 0 ) + "€" + " - " + $( "#slider-range" ).slider( "values", 1 ) + "€");

}