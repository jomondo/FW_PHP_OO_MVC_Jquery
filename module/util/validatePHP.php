<?php
    function validate_signup($value) {
        $error = array();
        $rdo = true;
        $filtro = array(
            'id' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z\sñÑ]{4,35}$/')
            ),
            'email' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{4,12}[.]{1}[A-Za-z]{2,4}$/')
            ),
            'password' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[A-Za-z0-9ñÑ]{5,20}$/')
            )
        );

        $result = filter_var_array($value, $filtro);

        if ($result != "" && $result) {
            $result['re_passwd']=$value['re_passwd'];

            if (!$result['id']) {
                $error['id'] = 'El nombre tiene que tener entre 4-35 caracteres';
                $rdo = false;

            }
            
            if (!$result['email']) {
                $error['email'] = 'Tiene que ser un email valido. ejemplo@gmail.com';
                $rdo = false;

            }

            if (!$result['password']) {
                $error['password'] = 'El password permite alfanumerico de 5-20 caracteres';
                $rdo = false;

            }

            if ($result['password'] != $result['re_passwd']) {
                $error['re_passwd'] = 'El password no coincide';
                $rdo = false;
            }

        }else {
            $rdo = false;

        }

        return $return = array('result' => $rdo, 'error' => $error, 'datos' => $result);
    }