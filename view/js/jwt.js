function do_encode(data){
	return getBase64_encode(JSON.stringify(data));
}

function do_decode(data){
	return getBase64_decode(data);
}

function getBase64_encode(data){
	var dataArr = CryptoJS.enc.Utf8.parse(data);
	var rdo = CryptoJS.enc.Base64.stringify(dataArr);

	return rdo;
}

function getBase64_decode(data){
	var dataArr = CryptoJS.enc.Base64.parse(data);
	var rdo = dataArr.toString(CryptoJS.enc.Utf8);

	return rdo;
}

function create_jwt(payload){
	var header = { "typ":"JWT","alg":"HS256"};
	var secret = "password1";

	var base64_header = getBase64_encode(JSON.stringify(header));
	//console.log(base64_header);
	var base64_payload = getBase64_encode(JSON.stringify(payload));

	var signature = CryptoJS.HmacSHA256(base64_header + "." + base64_payload, secret);
	var base64_sign = CryptoJS.enc.Base64.stringify(signature);

	var jwt = base64_header + "." + base64_payload + "." + base64_sign;
	console.log(jwt);
	return jwt;
}



//{ "typ":"JWT","alg":"HS256"}


////////////////////

/*function base64url(source) {
  // Encode in classical base64
  encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, '');

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, '-');
  encodedSource = encodedSource.replace(/\//g, '_');

  return encodedSource;
}

var source = "Hello!";

// 48 65 6c 6c 6f 21
console.log(CryptoJS.enc.Utf8.parse(source).toString());

var header = {
  "alg": "HS256",
  "typ": "JWT"
};

var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
var encodedHeader = base64url(stringifiedHeader);

var data = {
  "id": 1337,
  "username": "john.doe"
};

var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
var encodedData = base64url(stringifiedData);

var token = encodedHeader + "." + encodedData;
var secret = "My very confidential secret!";

var signature = CryptoJS.HmacSHA256(token, secret);
signature = base64url(signature);

var signedToken = token + "." + signature;


console.log(signedToken);*/
