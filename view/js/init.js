//modal
$( "#dialog" ).dialog({
    autoOpen: false,
    show: {
        effect: "blind",
        duration: 500
    },
    hide: {
        effect: "explode",
        duration: 500
    },
    modal: true,
    title: 'Login',
    width:550,
    height:615,
    resizable:false,
    buttons: {
        Exit: function() { $( this ).dialog( "close" ); },
    }
});
    
$( "#btn-signup" ).on( "click", function() {
    $( "#dialog" ).dialog( "open" );
    $(".ui-dialog-titlebar").css("background","#18ba60");
    $("form#change_password").css("display","none");

});
//form register
$( "#btn_register" ).on( "click", function() {
    $("#login").css("display","none");
    $("#btn_register").css("display","none");
    $("#register").css("display","block");

    //reset values (security)
    $("#name_signup").val('');
    $("#email_signup").val('');
    $("#passwd_signup").val('');
    $("#rePasswd_signup").val('');

});
//form login
$( "#btn-signup" ).on( "click", function() {
    $("form#register").css("display","none");
    $("form#login").css("display","block");
    $("#btn_register").css("display","block");


});
var update = localStorage.getItem("update");
if (update=="true") {
    amaran("Su perfil ha sido actualizado");
    localStorage.removeItem('update');
}

var verify_email = localStorage.getItem("verify_email");
if (verify_email) {
    amaran("Le hemos enviado un correo con el link para cambiar su contraseña.");
    localStorage.removeItem('verify_email');
}

var change = localStorage.getItem("change_passwd");
if (change) {
    amaran("Su contraseña ha sido actualizada satisfactoriamente.");
    localStorage.removeItem('change_passwd');
}

var activate = localStorage.getItem("activate");
if (activate) {
    amaran("Su cuenta ha sido verificada, ya puede disfrutar de nuestros servicios.");
    localStorage.removeItem('activate');
}

var aux = localStorage.getItem("aux");
if (aux=="reg") {
    amaran("Se le ha enviado un email para verificar su cuenta. Revisa su email para terminar el registro.");
    localStorage.removeItem('aux');
}
//init
var data = localStorage.getItem("datos_user");
alert(data);

if (data=="registrado") {
    //amaran("Te has registrado satisfactoriamente en nuestra web. Gracias por confiar con nosotros.");
    localStorage.removeItem('datos_user');
    $( "#dialog-message" ).css("display","block");
    $( "#dialog-message" ).dialog({
        modal: true,
        title: 'Register complete!',
        width:350,
        height:250,
        buttons: {
            Ok: function() {
                localStorage.clear();
                $("#btn-logout").css("display","none");
                window.location.href = amigable("?module=home");
            }
      }
    });

}else{
    if (data!==null) {
        //console.log(data);
        var json = JSON.parse(data);
        if (json.log_man==true) {
            $('div#btn-logout-manual').append('<button type="submit" class="btn btn-success " id="logout-manual">Logout</button>');
            $("#btn-signup").css("display","none");

        }
        var tokenInit = {"token":json.token};
        var tokenToInit = JSON.stringify(tokenInit);
        //alert(json.token);
        load_data_ajax("?module=login&function=select_user_token","POST",tokenToInit,"","select_user_token");

    }else{
        alert("eres usuari normal");
        $("#rating_freelancer").css("display","none");
        $('div.test_consultant').remove();

    }
}



