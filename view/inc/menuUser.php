<script src="https://cdn.auth0.com/js/auth0/9.2.2/auth0.min.js"></script>
<script src="<?php echo LOGIN_JS_PATH ?>login_socials_auth0.js"></script>
<script src="<?php echo LOGIN_JS_PATH ?>validate_signup.js"></script>
<script src="<?php echo LOGIN_JS_PATH ?>validate_login.js"></script>
<script src="<?php echo LOGIN_JS_PATH ?>change_passwd.js"></script>

<!-- header -->
<div class="header">
	<div class="agileits_top_menu">
		<div class="container">
			<div class="w3l_header_left">
				<ul>
					<li><i class="fa fa-phone" aria-hidden="true"></i> +34 624 046 466</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">info@freelancer.com</a></li>
					<li><i class="" aria-hidden="true"></i>© 2018 Classified Media Spain, S.L.</li>
				</ul>
			</div>
			<div class="w3l_header_right">
				<div class="w3ls-social-icons text-left">
					<button type="submit" class="btn btn-success " id="btn-signup" value="">Login</button>
					<div id="btn-logout-manual"></div>
					<button type="submit" class="btn btn-success " id="btn-logout">Logout</button>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="content white agile-info">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
					<a class="navbar-brand" href="<?php amigable('?module=home'); ?>">
						<h1>Freelancer</h1>
					</a>
				</div>
				<!--/.navbar-header-->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav"><!-- index.php?module=home&view=home" -->
							<li id="home" class="<?php if(@$_GET['module']== "home"){ echo "active";}else{ echo "";}?>"><a href="<?php amigable('?module=home'); ?>">Home</a></li>
							<li id="profile" class="<?php if(@$_GET['module']== "profile"){ echo "active";}else{ echo "";}?>"></li>
							<li class="<?php if(@$_GET['module']== "about"){ echo "active";}else{ echo "";}?>"><a href="<?php amigable('?module=about'); ?>">About</a></li>
							<li class="<?php if(@$_GET['module']== "services"){ echo "active";}else{ echo "";}?>"><a href="<?php amigable('?module=services'); ?>">Services</a></li>
							<li class="<?php if(@$_GET['module']== "contact"){ echo "active";}else{ echo "";}?>"><a href="<?php amigable('?module=contact'); ?>">Contact</a></li>
							<li id="avatar"></li>
							
						</ul>
					</nav>
				</div>
		
				<!-- modal signup -->
				<div id="dialog">
					<div class="form-group social_network">
						<div class="text-center center-block">
							<p class="listing"><i class="fa fa-chevron-right" aria-hidden="true"></i>Login network socials</p><br>
							<a href="" id="btn-login"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
							<a href="" id="btn-login"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
							<a href="" id="btn-login"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
						</div>
					</div><br>
					
					<form id="login">
						<p class="listing" align="center"><i class="fa fa-chevron-right" aria-hidden="true"></i>Login manual</p><br>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" name="" class="form-control" id="name_login" placeholder="write your name user">
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input type="password" name="" class="form-control" id="passwd_login" placeholder="write your password">
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-success" id="btn_submit_login" style="margin-left: 37.5%;width: 25%;">Log in</button>
							<p style="margin-right: 35%;margin-bottom: 22%;"><a class="pull-right text-muted" href="#" id="change_passwd"><small>Forgot your password?</small></a></p>
				        </div>
					</form>
					
					<button type="button" class="btn btn-success" id="btn_register" style="margin-left: 25%; width: 50%;">Sign up</button>

					<form id="register" style="display: none;">
						<p class="listing" align="center"><i class="fa fa-chevron-right" aria-hidden="true"></i>Register manual</p><br>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" name="" class="form-control" id="name_signup" placeholder="write your name user">
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
							<input type="text" name="" class="form-control" id="email_signup" placeholder="write your email">
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input type="password" name="" class="form-control" id="passwd_signup" placeholder="write your password">
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input type="password" name="" class="form-control" id="rePasswd_signup" placeholder="repeat your password">
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-success" id="btn_submit_signup">Register</button>
				        </div>
					</form>

					<!-- recovery_passwd -->
					<form id="change_password">
						<p class="listing" align="center"><i class="fa fa-chevron-right" aria-hidden="true"></i>Recovery password</p><br>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
							<input type="text" name="" class="form-control" id="email_passwd" placeholder="write your email">
						</div>
						
						<div class="form-group">
							<button type="button" class="btn btn-success" id="btn_passwd">Submit</button>
				        </div>
					</form>

				</div>

				<div id="dialog-message" title="Register complete" style="display: none;">
				  <p>
				    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
				    Bienvenido usuario. Te has registrado correctamente, logueate para poder utilizar la cuenta.
				  </p>
				</div>

				<!--/.navbar-collapse-->
				<!--/.navbar-->
			</div>
		</nav>
	</div>
</div>
<!-- //header -->

<!-- Breadcrumb  -->
<?php

	if( isset($_GET['module']) && !empty($_GET['function']) ){
		echo "<div class='banner'><h2>".$_GET['function']."</h2>";
		echo "<p><a href='https://localhost/freelancer/home/'>Main »</a><a href='https://localhost/freelancer/". $_GET['module'] ."/'>". $_GET['module'] ." »</a>". $_GET['function'] ."</p></div>";
		return false;

	}

	if (!isset($_GET['module'])) {//start web
		echo "";

	}else if ($_GET['module']=='home') {
		echo "";

	}else if(isset($_GET['module'])){
		echo "<div class='banner'><h2>".$_GET['module']."</h2>";
		echo "<p><a href='https://localhost/freelancer/home/'>Main »</a> ". $_GET['module'] ." </p></div>";

	}

	