<?php
    spl_autoload_register(null, false);
    spl_autoload_extensions('.php,.inc.php,.class.php,.class.singleton.php');

    spl_autoload_register('loadClasses');
    function loadClasses($className) {
        //print_r($className);
        $porciones = explode("_", $className);
        //print_r($porciones);
        $module_name = $porciones[0];
        $model_name = "";
        
        if(isset($porciones[1])){
            //print_r($porciones[1]);
            $model_name = $porciones[1];
            $model_name = strtoupper($model_name);

        }
            
        //profile && home
        if (file_exists('module/' . $module_name . '/model/'.$model_name.'/' . $className . '.class.singleton.php')) {
            set_include_path('module/' . $module_name . '/model/'.$model_name.'/');
            //print_r(get_include_path());
            spl_autoload($className);

        }
            
        //model
        elseif (file_exists('model/' . $className . '.class.singleton.php')) {
            set_include_path('model/');
            //print_r($className);
            spl_autoload($className);

        }
        
    }